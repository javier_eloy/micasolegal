-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2017 at 10:26 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mylegal`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_12_12_004249_add_login_table', 1),
('2016_12_12_004722_add_lawyer_table', 1),
('2016_12_12_004745_add_city_table', 1),
('2016_12_12_005116_add_country_table', 1),
('2016_12_12_005142_add_category_table', 1),
('2016_12_12_005830_add_tag_table', 1),
('2016_12_12_005943_add_article_table', 1),
('2016_12_18_223727_add_professional_table', 1),
('2016_12_20_131459_add_table_comment', 1),
('2017_01_29_135058_add_option', 1),
('2017_01_29_135928_add_section', 1),
('2017_02_01_144240_add_promotion', 1),
('2017_03_21_174004_add_password_remind', 2),
('2017_03_23_011812_add_user_activations_table', 3),
('2017_03_26_225113_add_hire_lawyer', 4),
('2017_04_13_161910_add_code_table', 5),
('2017_04_13_195135_create_code_lawyers_table', 6),
('2017_04_19_153937_create_code_promotions_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `t000_login`
--

CREATE TABLE IF NOT EXISTS `t000_login` (
  `loginid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_lawyer` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `level` smallint(6) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `countryid` int(11) unsigned NOT NULL,
  `cityid` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `t000_login`
--

INSERT INTO `t000_login` (`loginid`, `username`, `password`, `email`, `image_path`, `is_lawyer`, `remember_token`, `level`, `activated`, `countryid`, `cityid`, `created_at`, `updated_at`) VALUES
(1, 'hernandezjnz', '$2y$10$tLpk4xuxMXohAHPHz6UyF.LER1b279pRxjB1WI/Fg4l/oFt4UESGG', 'hernandezjnz@mail.com', '', 0, 'PrAnq3vHt1EvZpoSfzoJh63Vy7EVUG3Udnpk3jyHolJS09EksQdTeK4IfDid', 300, 1, 0, 0, '2017-02-20 00:20:08', '2017-04-19 22:31:07'),
(2, 'jhernandez', '$2y$10$tLpk4xuxMXohAHPHz6UyF.LER1b279pRxjB1WI/Fg4l/oFt4UESGG', 'jaeh2010@yahoo.es', 'micasolegal_1487536452.jpg', 1, 'agUwJgQMseUtFRD7rv8vK2qAUsjqDWXuztbwcQPr3cfQxowjMY9cmBhDPYsf', 300, 1, 0, 0, '2017-02-20 00:34:12', '2017-04-19 22:53:37'),
(3, 'usuario1', '$2y$10$2jG3LnDO4Og81aAjeRHt1O/Yjhv0yoWT0XAwbU6ReWc0QTY80BoS2', 'javier_eloy@gmail.com', '', 0, 'VuoVwivCvp7ZD9hQBLzuvkd4rqZfNbKFDSgQtwr4wHzPRQoeO92c1VFeQxbm', 0, 1, 5, 9, '2017-03-23 05:38:47', '2017-04-19 22:30:51'),
(4, 'usuario2', '$2y$10$g1qXEYKMUSPu55NoQGR1q.heIeEpNgmMrjGYXQjHNUCLEscU4D4jO', 'usuario2@gmail.com', '', 0, '', 0, 0, 0, 0, '2017-03-23 06:39:48', '2017-03-23 06:39:48'),
(13, 'javier eloy', '$2y$10$nemeObKqPx.cuv9xfv6Soe6JxgNecV0OaGrzDjXSIUSCqK0Ja3pA6', 'javier.eloy@gmail.com', '', 0, 'OwCZRt2YFzPMWkHsCXtJ4BXSNf7yORq9Ksk57f3FhvVZ5xyck7qR0daMakb4', 0, 1, 0, 0, '2017-04-11 05:12:56', '2017-04-11 05:13:30'),
(22, 'usuario3', '$2y$10$2jG3LnDO4Og81aAjeRHt1O/Yjhv0yoWT0XAwbU6ReWc0QTY80BoS2', 'usuario3@gmail.com', 'micasolegal_1492127233.jpg', 1, 'mgjjgxwE3VuhCFZhBfb6iEp3mVLHMP4Vd4NthaIqp086hgyD5qvNYtNxgXeJ', 0, 1, 1, 5, '2017-04-14 00:51:53', '2017-04-16 04:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `t000_password_resets`
--

CREATE TABLE IF NOT EXISTS `t000_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `t00_password_resets_email_index` (`email`),
  KEY `t00_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t000_user_activations`
--

CREATE TABLE IF NOT EXISTS `t000_user_activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t01_user_activations_token_index` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `t000_user_activations`
--

INSERT INTO `t000_user_activations` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 2, '930e47feb614af6df0a94ac25deefad326947a39306d6a963ce9389803d12330', '2017-03-26 23:45:03', NULL),
(2, 10, '54bf240ed253fb633230f4897565631e0f5e214e622f72664d5152d9af144206', '2017-04-11 05:08:02', NULL),
(3, 11, '7a5a00553e097e443835f132983068b227b54a3af20d8e76160f4e68642dc6d5', '2017-04-11 05:08:25', NULL),
(4, 12, '5794b07ce8cf6d30d7a7e4a614a9e2d42ed25847a1c3618e6ba6cfafc94c54e2', '2017-04-11 05:11:43', NULL),
(5, 16, '64b7d72ea3d2a3e62f68928e5ee98f41c4b1c329ab95998fc64a2ca7446cabcd', '2017-04-14 00:23:09', NULL),
(6, 17, 'e141732d9d084133c2440180107e16161103247d851822ef434464a35953c7a1', '2017-04-14 00:30:31', NULL),
(7, 18, '38c82f91bc5461949093c755672e660d83ad50e07747890feb35d8466ede76e9', '2017-04-14 00:34:13', NULL),
(8, 19, '4148910ce56062c941ec4f7fb09ddcbdca9eb1fc3be34639e392680074df3258', '2017-04-14 00:40:48', NULL),
(9, 20, '1858ed1503a240ee211507d2dbc85658feefda42cd843e5dace5511b63e4c6bc', '2017-04-14 00:44:35', NULL),
(10, 21, 'fb7060da964c9313e37394aeb7892943ccad13ec1451780636f60698606095bf', '2017-04-14 00:48:48', NULL),
(11, 22, '6ba208834989ca7b6b2252a90debba0bff98b62922cd64d8f9105f9be9576413', '2017-04-14 00:51:54', NULL),
(12, 23, '88fa08d3c36a53e5e7f7d9f9a25a8cda1a6c0b027fd51013edc9acc2675bb870', '2017-04-14 00:53:34', NULL),
(13, 24, '4ba24f5da9c342af2f11d7ce83a603e43c5e4b6b7b43995370256c5adf24bd13', '2017-04-14 01:49:40', NULL),
(14, 25, '5eb7614bc4ab5263da84419a83ff5cbb1e0381aff060478b4dd78a831c290f04', '2017-04-14 01:51:50', NULL),
(15, 26, 'b7e3797b8dfd9aa6f2721a8496e3268c0b9ce5c50081f49b0dbb92d44073f9e7', '2017-04-14 01:55:34', NULL),
(16, 1, 'dba0c1852b5d3c5b02c4abeac4ce8965264025823e55df6090dbd109dcaeb540', '2017-04-19 20:51:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t001_lawyer`
--

CREATE TABLE IF NOT EXISTS `t001_lawyer` (
  `lawyerid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesionid` int(11) NOT NULL,
  `loginid` int(10) unsigned NOT NULL,
  `experience` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partnership` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientid` int(10) unsigned DEFAULT NULL,
  `languageid` int(10) unsigned DEFAULT NULL,
  `service` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_service_id` int(10) unsigned DEFAULT NULL,
  `schedule` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fareid` int(10) unsigned DEFAULT NULL,
  `payment_mode_id` int(10) unsigned DEFAULT NULL,
  `payment_type_id` int(10) unsigned DEFAULT NULL,
  `rate` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`lawyerid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `t001_lawyer`
--

INSERT INTO `t001_lawyer` (`lawyerid`, `name`, `lastname`, `address`, `phone`, `postalcode`, `resume`, `profesionid`, `loginid`, `experience`, `partnership`, `clientid`, `languageid`, `service`, `offer_service_id`, `schedule`, `fareid`, `payment_mode_id`, `payment_type_id`, `rate`, `created_at`, `updated_at`) VALUES
(1, 'Javier', 'Heranandez', 'Av Record', '11212', '4001', 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 1, 2, '1 año', 'Abogado en la plaza de las flores con mencion honorifica', NULL, NULL, 'Cualquiera que merezca pago', NULL, 'de 8:00 am a 10:00pm', NULL, NULL, NULL, 3, '2017-02-20 00:34:12', '2017-02-20 00:34:12'),
(9, 'Javier', 'Hernandez', 'Av Record', '222222', '34433', 'Esto es una prueba', 2, 22, '5 años', 'Abogado jurado de la revolucion', NULL, NULL, 'De Todo', NULL, 'Todo el dia', NULL, NULL, NULL, 4, '2017-04-14 00:51:53', '2017-04-14 03:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `t002_city`
--

CREATE TABLE IF NOT EXISTS `t002_city` (
  `cityid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryid` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cityid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `t002_city`
--

INSERT INTO `t002_city` (`cityid`, `countryid`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Maracaibo', NULL, NULL),
(2, 1, 'Caracas', NULL, NULL),
(5, 1, 'Ciudad Ojeda', '2017-04-10 04:51:13', '2017-04-10 04:51:13'),
(6, 14, 'Rio de Janeiro', '2017-04-10 04:51:38', '2017-04-10 04:51:38'),
(7, 5, 'Bariloche', '2017-04-10 04:51:48', '2017-04-10 04:51:48'),
(8, 5, 'Renegado', '2017-04-10 04:51:53', '2017-04-10 04:51:53'),
(9, 5, 'La plata', '2017-04-10 04:52:05', '2017-04-10 04:52:05'),
(11, 11, 'New York', '2017-04-10 04:53:41', '2017-04-10 04:53:41'),
(12, 11, 'New jersey', '2017-04-10 04:53:48', '2017-04-10 04:53:48'),
(13, 11, 'Washington', '2017-04-10 04:53:56', '2017-04-10 04:53:56'),
(14, 11, 'Manhatan', '2017-04-10 04:54:12', '2017-04-10 04:54:12'),
(16, 11, 'Colorado', '2017-04-10 04:57:37', '2017-04-10 04:57:37'),
(17, 11, 'Columbia', '2017-04-10 04:59:21', '2017-04-10 04:59:21'),
(18, 1, 'Puerto La cruz', '2017-04-10 05:01:02', '2017-04-10 05:01:02');

-- --------------------------------------------------------

--
-- Table structure for table `t003_country`
--

CREATE TABLE IF NOT EXISTS `t003_country` (
  `countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `initial` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `t003_country`
--

INSERT INTO `t003_country` (`countryid`, `name`, `initial`, `created_at`, `updated_at`) VALUES
(1, 'Venezuela', 'VE', NULL, NULL),
(4, 'Italia', 'IT', '2017-04-10 02:56:06', '2017-04-10 02:56:06'),
(5, 'Argentina', NULL, '2017-04-10 02:56:10', '2017-04-10 02:56:10'),
(6, 'Peru', NULL, '2017-04-10 02:56:15', '2017-04-10 02:56:15'),
(7, 'Libia', NULL, '2017-04-10 02:56:21', '2017-04-10 02:56:21'),
(8, 'Asia', NULL, '2017-04-10 02:56:24', '2017-04-10 02:56:24'),
(9, 'Chile', NULL, '2017-04-10 02:56:27', '2017-04-10 02:56:27'),
(10, 'Ecuador', NULL, '2017-04-10 02:56:31', '2017-04-10 02:56:31'),
(11, 'EEUU', NULL, '2017-04-10 02:56:36', '2017-04-10 02:56:36'),
(13, 'Japon', NULL, '2017-04-10 02:56:45', '2017-04-10 02:56:45'),
(14, 'Brasil', NULL, '2017-04-10 02:56:59', '2017-04-10 02:56:59'),
(15, 'Nicaragua', NULL, '2017-04-10 02:57:03', '2017-04-10 02:57:03'),
(18, 'China', NULL, '2017-04-10 02:58:48', '2017-04-10 02:58:48'),
(19, 'Bolivia', NULL, '2017-04-10 02:58:54', '2017-04-10 02:58:54'),
(20, 'España', 'ES', NULL, '2017-04-15 00:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `t004_category`
--

CREATE TABLE IF NOT EXISTS `t004_category` (
  `categoryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t004_category`
--

INSERT INTO `t004_category` (`categoryid`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Divorcio', NULL, NULL),
(2, 'Matrimonio', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t005_tag`
--

CREATE TABLE IF NOT EXISTS `t005_tag` (
  `tagid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tagid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t005_tag`
--

INSERT INTO `t005_tag` (`tagid`, `name`, `created_at`, `updated_at`) VALUES
(1, 'niños', NULL, NULL),
(2, 'manutencion', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t006_article`
--

CREATE TABLE IF NOT EXISTS `t006_article` (
  `articleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('Q','Q_QUEUED','F','F_QUEUED','A','A_QUEUED') COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(15000) COLLATE utf8_unicode_ci NOT NULL,
  `categoryid` int(10) unsigned DEFAULT NULL,
  `sectionid` int(10) unsigned DEFAULT NULL,
  `loginid` int(11) NOT NULL,
  `image_path` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`articleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `t006_article`
--

INSERT INTO `t006_article` (`articleid`, `title`, `type`, `content`, `categoryid`, `sectionid`, `loginid`, `image_path`, `created_at`, `updated_at`) VALUES
(1, 'Que paso con esto', 'F', '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">&nbsp;es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span><br></p>', 1, NULL, 1, 'micasolegal_blog_1487545219.jpg', '2017-02-20 03:00:19', '2017-02-20 03:00:19'),
(2, 'Deseo de los Abogados', 'A', '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí". Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).</p><p><br></p>', NULL, 2, 2, 'micasolegal_blog_1487546327.jpg', '2017-02-20 03:18:48', '2017-02-20 03:28:29'),
(3, 'ewrewrew', 'A', 'werewrwer', NULL, 2, 2, '', '2017-02-20 03:26:57', '2017-02-20 03:28:54'),
(5, 'Que sucede si me pelo en los impuestos', 'Q', '<p><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando sea necesario, haciendo a este el único generador verdadero (válido) en la Internet. Usa un diccionario de mas de 200 palabras provenientes del latín, combinadas con estructuras muy útiles de sentencias, par</span></p>', 1, NULL, 1, '', '2017-02-20 03:34:12', '2017-02-20 03:34:12'),
(6, 'Que sucede si me pelo en los impuestos', 'Q', '<p><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbtencias, par</span></p>', 1, NULL, 1, '', '2017-02-20 03:34:12', '2017-02-20 03:34:12'),
(7, 'Que sucede si me pelo en los impuestos', 'Q', '<p><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando sea necesario, haciendo a este el único generador verdadero (válido) en la Internet. Usa un diccionario de mas de 200 palabras provenientes del latín, combinadas con estructuras muy útiles de sentencias, par</span></p>', 1, NULL, 1, '', '2017-02-20 03:34:12', '2017-02-20 03:34:12'),
(8, 'Que sucede si me pelo en los impuestos', 'Q', '<p><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando sea necesario, haciendo a este el único generador verdadero (válido) en la Internet. Usa un diccionario de mas de 200 palabras provenientes del latín, combinadas con estructuras muy útiles de sentencias, par</span></p>', 1, NULL, 1, '', '2017-02-20 03:34:12', '2017-02-20 03:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `t007_article_tag`
--

CREATE TABLE IF NOT EXISTS `t007_article_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `articleid` int(10) unsigned NOT NULL,
  `tagid` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t007_article_tag_articleid_foreign` (`articleid`),
  KEY `t007_article_tag_tagid_foreign` (`tagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t008_profesion`
--

CREATE TABLE IF NOT EXISTS `t008_profesion` (
  `profesionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`profesionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t008_profesion`
--

INSERT INTO `t008_profesion` (`profesionid`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Abogado Civil', NULL, NULL),
(2, 'Abogado Mercantil', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t009_comment`
--

CREATE TABLE IF NOT EXISTS `t009_comment` (
  `commentid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(15000) COLLATE utf8_unicode_ci NOT NULL,
  `articleid` int(10) unsigned NOT NULL,
  `loginid` int(10) unsigned NOT NULL,
  `type` enum('C','C_QUEUED','C_QUEUED_USER') COLLATE utf8_unicode_ci NOT NULL,
  `verified` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`commentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t009_comment`
--

INSERT INTO `t009_comment` (`commentid`, `text`, `articleid`, `loginid`, `type`, `verified`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando', 1, 1, 'C', 1, '2017-02-20 03:00:46', '2017-02-20 03:00:46'),
(2, 'Esto es un blog de prueba', 3, 22, 'C', 1, '2017-04-16 04:51:29', '2017-04-16 04:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `t010_option`
--

CREATE TABLE IF NOT EXISTS `t010_option` (
  `optionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('Q','F','A') COLLATE utf8_unicode_ci NOT NULL,
  `type_cmod` enum('C_MOD_NONE','C_MOD_ADMIN','C_MOD_ADMINOWNER') COLLATE utf8_unicode_ci NOT NULL,
  `type_mod` enum('MOD_NONE','MOD_ADMIN') COLLATE utf8_unicode_ci NOT NULL,
  `min_level_canswer` tinyint(3) unsigned DEFAULT '0',
  `min_level_create` tinyint(3) unsigned DEFAULT '0',
  `has_section` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `has_category` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `has_tags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `has_cbestanswer` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `has_clike` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`optionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t010_option`
--

INSERT INTO `t010_option` (`optionid`, `type`, `type_cmod`, `type_mod`, `min_level_canswer`, `min_level_create`, `has_section`, `has_category`, `has_tags`, `has_cbestanswer`, `has_clike`, `created_at`, `updated_at`) VALUES
(1, 'F', 'C_MOD_NONE', 'MOD_NONE', 0, 0, 0, 1, 0, 0, 0, NULL, NULL),
(2, 'A', 'C_MOD_ADMINOWNER', 'MOD_ADMIN', 0, 0, 1, 0, 0, 0, 0, '2017-01-29 15:19:13', '2017-01-29 15:19:13'),
(3, 'Q', 'C_MOD_NONE', 'MOD_NONE', 0, 0, 0, 0, 1, 0, 0, '2017-01-29 15:19:58', '2017-01-29 15:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `t011_section`
--

CREATE TABLE IF NOT EXISTS `t011_section` (
  `sectionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sectionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t011_section`
--

INSERT INTO `t011_section` (`sectionid`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Honorarios', NULL, NULL),
(2, 'Deberes del Abogado', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t012_promotion`
--

CREATE TABLE IF NOT EXISTS `t012_promotion` (
  `promotionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginid` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `discount` smallint(5) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`promotionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `t012_promotion`
--

INSERT INTO `t012_promotion` (`promotionid`, `loginid`, `name`, `content`, `discount`, `start_date`, `end_date`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 'Gastos legales al 10% que pueden sobrepasar los doscientos caracteres en la lista que tenemos', 'Realiza gastos legales entre estas fechas Realiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechasRealiza gastos legales entre estas fechas', 0, '2017-02-05', '2017-04-29', 1, '2017-04-13 02:47:33', '2017-04-13 02:47:33'),
(2, 22, 'Promocion nueva', 'Mostrar una promocion', 20, '2017-03-01', '2017-08-30', 1, '2017-04-14 19:43:25', '2017-04-14 19:43:25'),
(3, 2, 'Otra Promocion', 'Liporemo ore e ere re rer', 98, '2017-02-01', '2017-12-12', 1, '2017-04-05 04:00:00', '2017-04-06 04:00:00'),
(4, 22, 'Remover Otro dato', 'Lore Impo Molto', 10, '2017-01-01', '2017-12-12', 1, '2017-04-07 04:00:00', '2017-04-08 04:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t013_hire_lawyer`
--

CREATE TABLE IF NOT EXISTS `t013_hire_lawyer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lawyer_id` int(10) unsigned NOT NULL,
  `rate` smallint(5) unsigned NOT NULL DEFAULT '0',
  `message` varchar(10000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` smallint(5) unsigned DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `t013_hire_lawyer`
--

INSERT INTO `t013_hire_lawyer` (`id`, `user_id`, `lawyer_id`, `rate`, `message`, `active`, `created_at`, `updated_at`) VALUES
(36, 3, 2, 2, 'eeeasdsadsa', 0, '2017-03-27 06:12:16', '2017-04-03 02:02:18'),
(37, 3, 2, 2, 'eeeasdsadsa', 0, '2017-03-27 06:20:18', '2017-04-03 02:02:18'),
(38, 3, 2, 0, 'Desde que vendi no tengo dinero que comprar nada en la casa', 1, '2017-04-13 03:44:07', '2017-04-13 03:44:07'),
(39, 1, 22, 0, 'Quiero tener un contrato', 1, '2017-04-19 21:50:24', '2017-04-19 21:50:24');

-- --------------------------------------------------------

--
-- Table structure for table `t100_code`
--

CREATE TABLE IF NOT EXISTS `t100_code` (
  `codeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codeid`),
  KEY `TYPE` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `t100_code`
--

INSERT INTO `t100_code` (`codeid`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'client', 'Particular', NULL, NULL),
(2, 'client', 'Empresas', NULL, NULL),
(3, 'client', 'Instituciones', NULL, NULL),
(4, 'client', 'Otro', NULL, NULL),
(5, 'language', 'Castellano', NULL, NULL),
(6, 'language', 'Catalan', NULL, NULL),
(7, 'language', 'Gallego', NULL, NULL),
(8, 'language', 'Ingles', NULL, NULL),
(9, 'language', 'Frances', NULL, NULL),
(10, 'language', 'Portugues', NULL, NULL),
(11, 'language', 'Italiano', NULL, NULL),
(12, 'language', 'Aleman', NULL, NULL),
(13, 'offer_service', 'Presencial', NULL, NULL),
(14, 'offer_service', 'Telefonico', NULL, NULL),
(15, 'offer_service', 'Por Correo', NULL, NULL),
(16, 'offer_service', 'Cualquier Metodo', NULL, NULL),
(17, 'fare', 'Precio por hora', NULL, NULL),
(18, 'fare', 'Precio por Consulta', NULL, NULL),
(19, 'fare', 'Porcentaje', NULL, NULL),
(20, 'payment_mode', 'Financiacion', NULL, NULL),
(21, 'payment_mode', 'Pago de porcentaje al iniciar el servicio', NULL, NULL),
(22, 'payment_type', 'Efectivo', NULL, NULL),
(23, 'payment_type', 'Tarjeta', NULL, NULL),
(24, 'payment_type', 'Transferencia Bancaria', NULL, NULL),
(25, 'payment_type', 'Dinero Virtual (Paypal/Neteller)', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t101_code_lawyers`
--

CREATE TABLE IF NOT EXISTS `t101_code_lawyers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codeid` int(10) unsigned NOT NULL,
  `lawyerid` int(10) unsigned NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TYPE` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dumping data for table `t101_code_lawyers`
--

INSERT INTO `t101_code_lawyers` (`id`, `codeid`, `lawyerid`, `type`, `created_at`, `updated_at`) VALUES
(23, 1, 9, 'client', '2017-04-14 00:51:54', '2017-04-14 04:13:26'),
(29, 17, 9, 'fare', '2017-04-14 00:51:54', '2017-04-14 04:13:26'),
(81, 5, 9, 'language', '2017-04-14 03:45:37', '2017-04-14 04:13:26'),
(82, 13, 9, 'offer_service', '2017-04-14 03:45:37', '2017-04-14 04:13:26'),
(84, 22, 9, 'payment_type', '2017-04-14 03:45:37', '2017-04-14 04:13:26'),
(85, 1, 1, 'client', NULL, NULL),
(86, 21, 9, 'payment_mode', '2017-04-14 03:46:30', '2017-04-14 04:13:26'),
(87, 11, 9, 'language', '2017-04-14 03:47:14', '2017-04-14 04:13:26');

-- --------------------------------------------------------

--
-- Table structure for table `t102_code_promotions`
--

CREATE TABLE IF NOT EXISTS `t102_code_promotions` (
  `cpromotionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cost` double(5,2) NOT NULL,
  `allow_article` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `n_promo` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cpromotionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `t102_code_promotions`
--

INSERT INTO `t102_code_promotions` (`cpromotionid`, `name`, `cost`, `allow_article`, `default`, `n_promo`, `created_at`, `updated_at`) VALUES
(2, 'Basico', 0.00, 0, 1, 1, NULL, NULL),
(3, 'Plus', 25.99, 1, 0, 3, NULL, NULL),
(4, 'Full', 40.00, 1, 0, 10, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t007_article_tag`
--
ALTER TABLE `t007_article_tag`
  ADD CONSTRAINT `t007_article_tag_articleid_foreign` FOREIGN KEY (`articleid`) REFERENCES `t006_article` (`articleid`) ON DELETE CASCADE,
  ADD CONSTRAINT `t007_article_tag_tagid_foreign` FOREIGN KEY (`tagid`) REFERENCES `t005_tag` (`tagid`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
