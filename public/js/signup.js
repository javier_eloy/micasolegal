

$(document).ready( function (){
   
 
/*
*  Events Jquery
*
*/ 
   $('#is_lawyer').change(function() {	 
  
	   if (!$(this).prop('checked')) {
		   $('.is_lawyer_box').hide();
		   $('#name').removeAttr('required');
		   $('#lastname').removeAttr('required');
		   $('#address').removeAttr('required');
		   $('#profesionid').removeAttr('required');
		   $('#resume').removeAttr('required');
	   } else { 
		   $('.is_lawyer_box').show();
		   $('#name').prop('required',true);
		   $('#lastname').prop('required',true);
		   $('#address').prop('required',true);
		   $('#profesionid').prop('required',true);
		   $('#resume').prop('required',true);
		}
   });

   // Init values
   if($('#is_lawyer').is(':checked')) $('.is_lawyer_box').show();
								 else $('.is_lawyer_box').hide();

});