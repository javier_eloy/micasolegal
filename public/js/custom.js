


$(function(){	
	
    $('[data-toggle="confirmation"]').confirmation();
	
	$("#countryid").change(function() {
            $.getJSON(base_url + "/ajax-city/" + $("#countryid").val(), function(data) {
                var $cityid = $("#cityid");
                $cityid.empty();
                $.each(data, function(index, value) {
                    $cityid.append('<option value="' + index +'">' + value + '</option>');
                });
            });
    });
	
	$("#search_lawyer").on("submit", function(e) {
		  e.preventDefault();
		  $.ajax({
				type: "POST",
				url: base_url + "/ajax-lawyersearch",
				data: $(this).serialize(),
				beforeSend: function () {
					$("[data-result]").html('<div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" ></i></div>');
				},
				success: function (xhr) {
					if(xhr.success)
						 $("[data-result]").html(xhr.html);
					else $("[data-result]").html('No existen abogados según su filtro');
				}
			  
		  });
		
	});

	$("input#file_show").on("change", function() {
		
		if(this.files && this.files[0])
		{
			var reader = new FileReader();
			reader.onload = function(e)
			{
				$('#image_show').attr('src',e.target.result);	
			}
			reader.readAsDataURL(this.files[0]);
		}
	});
		
	$("#close-banner").on("click",function() {
		$(".banner").slideUp(900);
	});
	
	$("[data-charcounter]").charCounter(10000, {
		classname: "counter",
		format: "(%1 caracter restantes)",
		pulse: false,
		delay: 10
	});
	
	// Start init values
	if($("#countryid").length && $("#countryid").data("changeinit")) {
		$("#countryid").trigger('change');
	}
	$('body').readMore();
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
	$('[data-toggle="tooltip"]').tooltip();   

});