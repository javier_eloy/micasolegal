@extends('template.main')

 
@section('content')

  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Login</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Login</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
		<section id="login" class="login-background fill" style="background-image:url('{{ asset('images/banner-login.png') }}'); background-size: cover; ">
			<div class="row">
				<div class="col-md-4 col-md-push-4 col-sm-4 col-sm-push-4 col-xs-6 col-xs-push-3">
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif  
				
				 {!! Form::open(['route' => 'auth.login', 'method' => 'POST']) !!}
					{{ csrf_field() }}
					<div class="form-group">
						{!! Form::label('username', 'Nombre del Usuario') !!}
						{!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Nombre del usuario', 'required']) !!}
					</div>
					
					<div class="form-group">
						{!! Form::label('password', 'Clave de ingreso') !!}
						{!! Form::password('password', ['class' => 'form-control', 'required']) !!}
					</div>	
					
					<div class="form-group text-center">
						{!! Form::submit('Entrar', ['class' => 'btn btn-primary']) !!}
						&nbsp;
						<a class="btn btn-success" href="{{ route('user.create')}}"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> Registrate</a>
						<a class="login" href="{{ url('password/reset')}}">¿Olvidó la clave?</a>
						
					</div>
				{!! Form::close() !!}
				</div>
			</div>
			<!-- /.row -->
		</section>
    </div>
    <!-- /.container -->
	 
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
@endsection