   <!-- Sidebar Column -->

   @if(isset($sections))
    <div class="col-md-3">
		<div class="list-group">
		<a href="{{ route('blog.index')}}" class="list-group-item {!! Route::is('blog.index') ? 'active' : '' !!}">Todos los Articulos</a>
		@foreach($sections as $section)
			<a href="{{ route('blog.section', $section->sectionid) }}" class="list-group-item {!! (Route::getCurrentRequest()->getUri() == route('blog.section', $section->sectionid)) ? 'active' : '' !!}">{{ $section->name }}</a>
		@endforeach	
		</div>
	</div>
   @endif