<footer class="short" id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<a href="demo-law-firm.html" class="logo mb-md">
								<img alt="Porto Website Template" class="img-responsive" width="97" height="32" src="img/demos/law-firm/logo-law-firm-footer.png">
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis.</p>
						</div>
						<div class="col-md-3 col-md-offset-1">
							<h5 class="mb-sm">Porto Law Firm</h5>
							<ul class="list list-icons mt-xl">
								<li><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</li>
								<li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h5 class="mb-sm">Llama</h5>
							<span class="phone">(800) 123-4567</span>
							<ul class="social-icons mt-lg">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<p>&copy; Copyright Mi Caso Legal 2016. | <a href="{{ route('contact.index')}}">Contactanos</a> | <a href="{{ route('faq.index')}}">FAQ</a></p>
							</div>
						</div>
					</div>
				</div>
</footer>
<!-- Footer -->
