   <!-- Sidebar Column -->
	<div class="col-md-3">
		<div class="list-group">
			<a href="{{ route('user.show',Auth::user()->loginid) }}" class="list-group-item {!! Route::is('user.show') ? 'active' : '' !!}">Mis Estadisticas</a>
			<a href="{{ route('user.mypost') }}" class="list-group-item {!! Route::is('user.mypost') ? 'active' : '' !!}">Mis Preguntas</a>
			<a href="{{ route('user.myanswer') }}" class="list-group-item {!! Route::is('user.myanswer') ? 'active' : '' !!}">Mis Respuestas</a>
			<a href="{{ route('user.mymoderate') }}" class="list-group-item {!! Route::is('user.mymoderate') ? 'active' : '' !!}">Moderaciones</a>
			<a href="{{ route('user.mypromotion') }}" class="list-group-item {!! Route::is('user.mypromotion') ? 'active' : '' !!}">Promociones</a>						
			<a href="{{ route('user.myhire') }}" class="list-group-item {!! Route::is('user.myhire') ? 'active' : '' !!}">Servicios Contratados</a>			
			<a href="{{ route('user.whohire') }}" class="list-group-item {!! Route::is('user.whohire') ? 'active' : '' !!}">¿Quien me contrato?</a>			
		</div>
	</div>