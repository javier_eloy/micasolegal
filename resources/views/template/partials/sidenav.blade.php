  <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin.index') }}">Administración</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li>
                    <a href="{{ route('index') }}" ><i class="fa fa-home fa-fw"></i>Pagina Principal</a>                    
                </li>
                                
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="{{ route('admin.profiles') }}"><i class="fa fa-fw fa-group"></i> Usuarios</a>
                    </li>
                   <li>
                        <a href="{{ route('admin.config')}}"><i class="fa fa-fw fa-wrench"></i> Configuraci&oacute;n</a>
                    </li>
                   <li>
                        <a href="{{ route('admin.promotion')}}"><i class="fa fa-fw fa-table"></i> Promociones</a>
                    </li>
                   <!--  <li>
                        <a href="forms.html"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>
                    <li>
                        <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li>
                        <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>                                        -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>