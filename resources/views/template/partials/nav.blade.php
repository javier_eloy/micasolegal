	<header id="header" class="header-no-border-bottom" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 115, 'stickySetTop': '-115px', 'stickyChangeLogo': false}" style="min-height: 175px;">
				<div class="header-body" style="top: 0px;">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="{{ route('index') }}">
										<img alt="Porto" width="164" height="54" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="img/demos/law-firm/logo-law-firm.png">
									</a>
								</div>
							</div>
							<div class="header-column">
								<ul class="header-extra-info">
									<li>
										<div class="feature-box feature-box-call feature-box-style-2">
											<div class="feature-box-icon">
												<i class="icon-call-end icons"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="mb-none">(800) 123-4567</h4>
											</div>
										</div>
									</li>
									<li class="hidden-xs">
										<div class="feature-box feature-box-mail feature-box-style-2">
											<div class="feature-box-icon">
												<i class="icon-envelope icons"></i>
											</div>
											<div class="feature-box-info">
												<h4 class="mb-none"><a href="mailto:mail@example.com">mail@example.com</a></h4>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="header-container header-nav header-nav-bar header-nav-bar-primary">
						<div class="container">
							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
								Menu <i class="fa fa-bars"></i>
							</button>
							<div class="header-search visible-lg">
								<form id="searchForm" action="page-search-results.html" method="get" novalidate="novalidate">
									<div class="input-group">
										<input type="text" class="form-control" name="q" id="q" placeholder="Search..." required="" aria-required="true">
										<span class="input-group-btn">
											<button class="btn btn-default" type="submit"><i class="icon-magnifier icons"></i></button>
										</span>
									</div>
								</form>
							</div>
							<div class="header-nav-main header-nav-main-light header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse" aria-expanded="false" style="height: 0px;">
								<nav>
									<ul class="nav nav-pills" id="mainNav">
										<!-- <li class="{!! Route::is('promotion.index') ? 'active' : '' !!}" >
											<a href="{{ route('promotion.index')}}"  title="Promociones" ></a>
										</li>
										-->
										<li class="{!! Route::is('index') ? 'active' : '' !!}" >
											<a href="{{ route('index')}}" >Home</a>
										</li>
										<li class="{!! Route::is('about') ? 'active' : '' !!}" >
											<a href="{{ route('about')}}" >Quienes Somos</a>
										</li>
										<li class="{!! Route::is('lawyer.*') ? 'active' : '' !!}">
											<a href="{{ route('lawyer.index')}}">Abogados</a>
										</li>
										<li class="{!! Route::is('blog.*') ? 'active' : '' !!}">
											<a href="{{ route('blog.index') }}">Articulos</a>
										</li>					
										<li class="{!! Route::is('forum.*') ? 'active' : '' !!}">
											<a href="{{ route('forum.index') }}">Comunidad</a>
										</li>					

										<li class="{!! Route::is('question.*') ? 'active' : '' !!}">
											<a href="{{ route('question.index') }}">Consultas en Linea</a>
										</li>
										
										<li class="dropdown">
											@if(Auth::guest())
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios </a>
											<ul class="dropdown-menu">
												<li>
												  <a href="{{ route('auth.login') }}"><i class="fa fa-sign-in fa-fw" aria-hidden="true"></i> Login</a>
												</li>								
												<li>
													<a href="{{ route('user.create')}}"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> Registrate</a>
												</li>                            
											</ul>
											@else
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->username}} <b class="caret"></b></a>
											<ul class="dropdown-menu">

												<li>
													<a href="{{ route('user.edit',Auth::user()->loginid) }}"><i class="fa fa-user fa-fw" aria-hidden="true"></i> Mi cuenta</a>
												</li>
												<li>
													<a href="{{ route('user.show',Auth::user()->loginid) }}"><i class="fa fa-newspaper-o  fa-fw" aria-hidden="true"></i> Mi Actividad</a>
												</li>							
												@if(Auth::user()->level == LEVEL_ADMIN)
												<li class="divider"></li>
												<li>
													<a href="{{ route('admin.index') }}">Administracion</a>						
												</li>
												@endif
												<li>
												  <a href="{{ route('auth.logout') }}">Logout</a>
												</li>
											</ul>
											@endif						
										</li>									
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
	</header>
			
   