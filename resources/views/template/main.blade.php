<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mi Caso Legal</title>
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
	
	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/animate/animate.min.css') }}" >
	<link rel="stylesheet" href="{{ asset('plugins/simple-line-icons/css/simple-line-icons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/owl.carousel/assets/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/owl.carousel/assets/owl.theme.default.min.css') }}" >
	<link rel="stylesheet" href="{{ asset('plugins/magnific-popup/magnific-popup.min.css') }}" >

    <!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/chosen/chosen.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/jquery-readmore/css/advanced-read-more.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/jquery-starrating/css/star-rating.min.css') }}">
	
	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ asset('css/theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/theme-elements.css') }}">
	<link rel="stylesheet" href="{{ asset('css/theme-blog.css') }}">
	<link rel="stylesheet" href="{{ asset('css/theme-shop.css') }}">
	
    <!-- Custom CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/rs-plugin/css/settings.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/rs-plugin/css/layers.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/rs-plugin/css/navigation.css') }}">

	
	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{ asset('css/skins/skin-law-firm.css') }}"> 

	<!-- Demo CSS -->
	<link rel="stylesheet" href="{{ asset('css/skins/demo-law-firm.css') }}">
	
	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">


	<!-- <link rel="stylesheet" href="{{ asset('css/modern-business.css') }}">-->
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('css/general.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}">-->

    <!-- Custom Fonts -->
	<link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="{{ asset('plugins/modernizr/modernizr.min.js') }}"></script>
	
</head>

<body>
	<div class="body">
	   @include('template.partials.nav')
	   @include('flash::message')
	   @yield('content')   
	 </div>
	   @include('template.partials.footer')
   

	<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery-readmore/js/advanced-read-more.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery-charcounter/jquery.charcounter.js') }}"></script>
	<script src="{{ asset('plugins/jquery-starrating/js/star-rating.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery.appear/jquery.appear.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery.easing/jquery.easing.min.js') }}"></script>
	<script src="{{ asset('plugins/jquery-cookie/jquery-cookie.min.js') }}"></script>

	<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap/js/bootstrap-confirmation.min.js') }}"></script>
	
	<script src="{{ asset('plugins/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
	<script src="{{ asset('plugins/isotope/jquery.isotope.min.js') }}"></script>
	<script src="{{ asset('plugins/owl.carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('plugins/vide/vide.min.js') }}"></script>

	<script src="{{ asset('js/common.min.js') }}"></script>
	
	<!-- Theme Initialization Files -->
	<script src="{{ asset('js/theme.js') }}"></script>
	<!-- Current Page Vendor and Views -->
	<script src="{{ asset('plugins/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
	<script src="{{ asset('plugins/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>	

	<script src="{{ asset('js/demo-law-firm.js') }}"></script>	

	@yield('js')

	<script src="{{ asset('js/theme.init.js') }}"></script>

	<!-- <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
	<script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script> -->
	
	<script type="text/javascript"> 
	   var base_url = '{!! url('/') !!}';
	   var localization;
	   
	   $.get("https://ipinfo.io", function(response) {
				localization = response.country;
			}, "jsonp");
	</script>
</body>

</html>
