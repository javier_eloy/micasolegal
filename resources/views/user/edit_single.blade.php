@extends('template.main')

 
@section('content')

  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Cuenta</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Cuenta</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-12">
				<div class="row">					
				  <div class="col-md-12">
			
					{!! Form::open(['route' => ['user.update',$user], 'method' => 'PUT', 'files' => 'true']) !!}
					<div class="row">
						<div class="col-md-12">
						@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
						@endif
						</div>
					 <div class="col-md-push-6 col-md-6 text-center"> 
							<br/>
							<div class="form-group text-center">
								<label class="btn btn-primary btn-file">
									Cargar Imagen {!! Form::file('image',['id' => 'file_show', 'style'=> 'display:none']) !!}
								</label>		
								
								<img id="image_show" class="img-responsive" width="400px" src='{!! ($user->image_path) ? asset("images/user/".$user->image_path) :  asset("/images/default-user.png") !!}' />
							</div>
					</div> 
					
					 <div class="col-md-pull-6 col-md-6">
						<br>
						<div class="form-group">
							{!! Form::label('username', 'Nombre del Usuario') !!}
							<div class="form-control-static text-muted">{{ $user->username }}</div>					
						</div>
						
						<div class="form-group">
							{!! Form::label('email', 'Correo Electronico') !!}
							<div class="form-control-static text-muted">{{ $user->email }}</div>					
						</div>
										
						<div class="form-group">
							{!! Form::label('password', 'Clave de ingreso') !!}
							{!! Form::password('password', ['class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							{!! Form::label('password_confirmation', 'Retipee la clave de ingreso') !!}
							{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
						</div>				
						
						<div class="form-group">
							{!! Form::label('countryid', 'Pais') !!}
							{!! Form::select('countryid', $country ,  $user->countryid, ['class' => 'form-control']) !!}
						</div>
						
						<div class="form-group">
							{!! Form::label('cityid', 'Ciudad/Provincia') !!}
							{!! Form::select('cityid', $city, $user->cityid, ['class' => 'form-control']) !!}
						</div>
									
					</div>
					
					
					<div class="col-md-12">			
						<div class="form-group">
							{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}					
						</div>			
					</div>											
					
				 </div>
				{!! Form::close() !!}
			   </div>
			</div>	
           </div>
        </div>
        <!-- /.row -->
		
    </div>
    <!-- /.container -->
	 
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
@endsection