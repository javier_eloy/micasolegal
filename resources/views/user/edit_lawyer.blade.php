@extends('template.main')

 
@section('content')

  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Cuenta</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Cuenta</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-12">
			
			<ul id="myTab" class="nav nav-tabs nav-justified">
				<li class=""><a href="#service-one" data-toggle="tab"><i class="fa fa-graduation-cap"></i> Mejora tu nivel</a>
				</li>
				<li class="active"><a href="#service-two" data-toggle="tab"><i class="fa fa-edit"></i> Mis Datos</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content">
				
			   <div class="tab-pane" id="service-one">
			   <div class="row">
			   <br><br>
			    @foreach($code_promotion As $code_p)
					<div class="col-md-4">
						<div class="panel panel-default text-center">
							<div class="panel-heading">
								<h3 class="panel-title">{!!  $code_p->name !!}</h3>
							</div>
							<div class="panel-body">
								<span class="price"><sup>$</sup>{{ $code_p->cost }}</span>
								<span class="period">Anual</span>
							</div>
							<ul class="list-group">
								<li class="list-group-item"><strong class="lead" >{{ $code_p->n_promo }}</strong> Promociones Máxima</li>
								<li class="list-group-item">{{ ($code_p->allow_article == 1) ? "SI" : "NO" }} permite crear articulos </li>
								<li class="list-group-item"><a href="#" class="btn btn-primary">Registrar!</a>
								</li>
							</ul>
						</div>
					</div>
				@endforeach
					
			</div>
			   
			   
			</div>
			
            <div class="tab-pane fade active in" id="service-two">
			{!! Form::open(['route' => ['user.update',$user], 'method' => 'PUT', 'files' => 'true']) !!}
			{!! Form::hidden('is_lawyer',1) !!}

				<div class="row">
					
				<div class="col-md-12">
				   @if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
					
				<div class="col-md-push-6 col-md-6 text-center"> 
					<br/>
					<div class="form-group text-center">
						<label class="btn btn-primary btn-file">
							Cargar Imagen {!! Form::file('image',['id' => 'file_show', 'style'=> 'display:none']) !!}
						</label>		
						
						<img id="image_show" class="img-responsive" width="400px" src='{!! ($user->image_path) ? asset("images/user/".$user->image_path) :  asset("/images/default-user.png") !!}' />
					</div>
				</div> 
				
				 <div class="col-md-pull-6 col-md-6">
					<br>
					<div class="form-group">
						{!! Form::label('username', 'Nombre del Usuario') !!}
						<div class="form-control-static text-muted">{{ $user->username }}</div>					
					</div>
					
					<div class="form-group">
						{!! Form::label('email', 'Correo Electronico') !!}
						<div class="form-control-static text-muted">{{ $user->email }}</div>					
					</div>
									
					<div class="form-group">
						{!! Form::label('password', 'Clave de ingreso') !!}
						{!! Form::password('password', ['class' => 'form-control']) !!}
					</div>
					
					<div class="form-group">
						{!! Form::label('password_confirmation', 'Retipee la clave de ingreso') !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
					</div>				
					
					<div class="form-group">
						{!! Form::label('countryid', 'Pais') !!}
						{!! Form::select('countryid', $country ,  $user->countryid, ['class' => 'form-control']) !!}
					</div>
					
					<div class="form-group">
						{!! Form::label('cityid', 'Ciudad/Provincia') !!}
						{!! Form::select('cityid', $city, $user->cityid, ['class' => 'form-control']) !!}
					</div>
								
				</div>
				
				<div class="col-md-12">

					<div class="row">
						   <div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading text-center">
									<strong>Datos Adicionales</strong>
								</div>
								<div class="panel-body">
									<div class="form-group">
										{!! Form::label('name', 'Nombres') !!}
										{!! Form::text('name', $user->lawyer->name, ['class' => 'form-control', 'placeholder' => 'Nombres']) !!}
									</div>
									
									<div class="form-group">
										{!! Form::label('lastname', 'Apellidos') !!}
										{!! Form::text('lastname', $user->lawyer->lastname, ['class' => 'form-control', 'placeholder' => 'Apellidos']) !!}
									</div>		

									<div class="form-group">
										{!! Form::label('phone', 'Telefono') !!}
										{!! Form::text('phone', $user->lawyer->phone, ['class' => 'form-control', 'placeholder' => 'Telefono']) !!}
									</div>		

									<div class="form-group">
										{!! Form::label('profesionid', 'Profesión') !!}
										{!! Form::select('profesionid', $profesion ,  $user->lawyer->profesionid, ['class' => 'form-control']) !!}
									</div>

									<div class="form-group">
										{!! Form::label('postalcode', 'Codigo Postal') !!}
										{!! Form::text('postalcode', $user->lawyer->postalcode, ['class' => 'form-control', 'placeholder' => 'Codigo Postal']) !!}
									</div>		

									<div class="form-group">
										{!! Form::label('address', 'Direccion Actual') !!}
										{!! Form::text('address',  $user->lawyer->address, ['class' => 'form-control', 'placeholder' => 'Direccion Actual']) !!}
									</div>		
									
									<div class="form-group">
										{!! Form::label('resume', 'Resumen Curricular') !!}
										{!! Form::textarea('resume',  $user->lawyer->resume, ['class' => 'form-control textarea-content', 'maxlength' => '1000']) !!}
									</div>
								 </div>
							 </div>
						  </div> 
						  <div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading text-center">
									<strong>Servicios y Detalles de Pago</strong>
								</div>
								<div class="panel-body">
									<div class="form-group">
										{!! Form::label('experience', 'Años de Experiencia') !!}
										{!! Form::text('experience', $user->lawyer->experience, ['class' => 'form-control', 'maxlength' => '50']) !!}
									</div>	
									
									<div class="form-group">
										{!! Form::label('partnership', '¿Has tenido reconocimientos?') !!}
										{!! Form::textarea('partnership', $user->lawyer->partnership, ['class' => 'form-control', 'maxlength' => '500', 'rows' => '3', 'style' => 'resize:vertical']) !!}
									</div>
									
									<div class="form-group">							
										{!! Form::label('special', '¿En cuales areas te especializas?') !!}
										<br>
										@foreach($specialities as $special)
											<span class='text-nowrap'>{!! Form::checkbox('special[]', $special->codeid, isset($load_code[$special->codeid]) ) !!} {{ $special->value }}</span>
										@endforeach								
									</div>
									
									<div class="form-group">							
										{!! Form::label('client', '¿Cuales son tus principales clientes?') !!}
										<br>
										@foreach($client as $cl)
											{!! Form::checkbox('client[]', $cl->codeid, isset($load_code[$cl->codeid]) ) !!} {{ $cl->value }}
										@endforeach								
									</div>
									
									<div class="form-group">							
										{!! Form::label('language', 'Idiomas que maneja') !!}
										<br>
										@foreach($language as $lang)
											{!! Form::checkbox('language[]', $lang->codeid, isset($load_code[$lang->codeid]) ) !!} {{ $lang->value }}
										@endforeach								
									</div>
									
									<div class="form-group">
										{!! Form::label('service', '¿Que tipo de servicios ofrece?') !!}
										{!! Form::textarea('service', $user->lawyer->service, ['class' => 'form-control', 'maxlength' => '500', 'rows' => '3', 'style' => 'resize:vertical']) !!}
									</div>
									
									<div class="form-group">							
										{!! Form::label('offer_service', '¿Como ofrece el servicio?') !!}
										<br>
										@foreach($service as $srv)
											{!! Form::checkbox('offer_service[]', $srv->codeid, isset($load_code[$srv->codeid])) !!} {{ $srv->value }}
										@endforeach								
									</div>
									
									<div class="form-group">
										{!! Form::label('schedule', '¿Cual es tu horario?') !!}
										{!! Form::text('schedule', $user->lawyer->schedule, ['class' => 'form-control', 'maxlength' => '50']) !!}
									</div>
									
									<div class="form-group">							
										{!! Form::label('fare', '¿Como son tus tarifas?') !!}
										<br>
										@foreach($fare as $fr)
											{!! Form::checkbox('fare[]', $fr->codeid, isset($load_code[$fr->codeid])) !!} {{ $fr->value }}
										@endforeach								
									</div>
									
									<div class="form-group">							
										{!! Form::label('fare', '¿Ofrece facilidad en la forma de pago?') !!}
										<br>
										@foreach($payment_mode as $pm)
											{!! Form::checkbox('payment_mode[]', $pm->codeid, isset($load_code[$pm->codeid])) !!} {{ $pm->value }}
										@endforeach								
									</div>
									
									<div class="form-group">							
										{!! Form::label('payment_type', '¿Cual es su modalidad de pago?') !!}
										<br>
										@foreach($payment_type as $pt)
											{!! Form::checkbox('payment_type[]', $pt->codeid, isset($load_code[$pt->codeid])) !!} {{ $pt->value }}
										@endforeach								
									</div>
								</div>
							</div>
						 </div>
					</div>
				</div>
				
				<div class="col-md-12">			
					<div class="form-group">
						{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}					
					</div>			
				</div>
				 
				</div>
				
				{!! Form::close() !!}			
				 </div>			 
			 <!-- End service two -->			
			</div>				
           </div>
        </div>
        <!-- /.row -->
		
    </div>
    <!-- /.container -->
	 
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
@endsection