@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad
                    <small>Eventos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
			<!-- Posted Comments -->
			<div class="col-md-9">
			@foreach($articles as $article)
				 <div class="row">				   
					<div class="col-md-12">
						<span><a href="{{ route('question.show',$article->articleid) }}">{{ $article->title }}</a></span>						
						<p><strong>Respuestas:</strong></p>
						<div class="row">						
							<div class="col-md-11 col-md-offset-1"> 
							  <ul class="list-unstyled">
							  @foreach($article->comment as $comment)
								 <li> 
								    <i class="fa fa-clock-o"></i>
									<small>{{ $comment->created_at->diffForHumans() }}:</small>
									<span>{{ $comment->ShortText }} </span>
								 </li>
								@endforeach 
							</ul>
							</div>
						</div>
					</div>
				 </div>
				 <hr>
			@endforeach
			</div>			
			{{ $articles->links() }}
        </div>
        <!-- /.row -->

    </div>


@endsection