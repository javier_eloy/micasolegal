@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad
                    <small>Eventos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
			
			@if(count($cmoderates))
				<!-- Posted Articles by User-->
				<div class="col-md-9 pull-right">
				<h3>Articulos/Consulta/Foro:</h3>
				<hr>
				@foreach($cmoderates as $moderate)
					 <div class="row">				   
						<div class="col-md-11">
							<small><strong>Titulo del {{ $moderate->article->TranslateType }}:</strong> {{ $moderate->article->title }}</small><br>
							<i class="fa fa-clock-o"></i>
							<small>{{ $moderate->created_at->diffForHumans() }}</small>					
							<p>
							{!! $moderate->text !!}
							</p>
						</div>
						<div class="col-md-1">
						 <a class="btn btn-primary" href="{{ route('user.c_aprove',$moderate->commentid) }}" title="Aprobar"><i class="fa fa-check-square-o fa-fw"></i></a>
						 <a class="btn btn-danger" href="{{ route('user.c_reject',$moderate->commentid) }}" title="Eliminar"><i class="fa fa-share-square-o fa-fw"></i></a>
						</div>
					 </div>
					 <hr>
				@endforeach
				</div>	
			@endif
			
			@if(\Auth::user()->level == LEVEL_ADMIN)
					<!-- Posted Articles by Admin-->
					@if(count($admin_amoderates) || count($admin_cmoderates) )
						<div class="col-md-9 pull-right">	
						<h2>Moderacion del Administrador:</h2>
						</div>
					@endif
					
					@if(count($admin_amoderates))
					<div class="col-md-9 pull-right">					
					<h3>Articulos/Consulta/Foro:</h3>
					<hr>
					@foreach($admin_amoderates as $moderate)
						 <div class="row">				   
							<div class="col-md-11">
								<strong>Tipo:</strong> {{ $moderate->TranslateType }}<br>
								<i class="fa fa-clock-o"></i>
								<small>{{ $moderate->created_at->diffForHumans() }}</small>								
								<p>
								<strong>Titulo:</strong>
								{{ $moderate->title }}
								</p>
								<p>
								<strong>Contenido:</strong>
								{!! $moderate->content !!}
								</p>
							</div>
							<div class="col-md-1">
								<a class="btn btn-primary" href="{{ route('user.a_aprove',$moderate->articleid) }}" title="Aprobar"><i class="fa fa-check-square-o fa-fw"></i></a>
								<a class="btn btn-danger" href="{{ route('user.a_reject',$moderate->articleid) }}" title="Eliminar"><i class="fa fa-share-square-o fa-fw"></i></a>
							</div>
						 </div>
						 <hr>
					@endforeach					
					</div>	
					@endif
					
					<!-- Posted Comment by Admin-->
					@if(count($admin_cmoderates))
						<div class="col-md-9 pull-right">
						<h3>Comentarios:</h3>
						<hr>
						@foreach($admin_cmoderates as $moderate)
							 <div class="row">				   
								<div class="col-md-11">
									<p> 									
									<small><strong>Titulo del {{ $moderate->article->TranslateType}}:</strong> {{ $moderate->article->title }}</small><br>
									<i class="fa fa-clock-o"></i>									
									<small>{{ $moderate->created_at->diffForHumans() }}</small>
									<p>
									{!! $moderate->text !!}
									</p>
								</div>
								<div class="col-md-1">
									<a class="btn btn-primary" href="{{ route('user.c_aprove',$moderate->commentid) }}" title="Aprobar"><i class="fa fa-check-square-o fa-fw"></i></a>
									<a class="btn btn-danger" href="{{ route('user.c_reject',$moderate->commentid) }}" title="Eliminar"><i class="fa fa-share-square-o fa-fw"></i></a>
								</div>
							 </div>
							 <hr>
						@endforeach
						</div>	
					@endif	
			@endif
        </div>
        <!-- /.row -->

    </div>


@endsection