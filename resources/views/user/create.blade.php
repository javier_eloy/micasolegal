@extends('template.main')

 
@section('content')

  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Registro</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Registro</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
		{!! Form::open(['route' => 'user.store', 'method' => 'POST', 'files' => 'true']) !!}

        <div class="row">
			
		<div class="col-md-12">
		   @if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
			
		<div class="col-md-push-6 col-md-6 text-center"> 
			<div class="form-group text-center">
				<label class="btn btn-primary btn-file">
					Cargar Imagen {!! Form::file('image',['id' => 'file_show', 'style'=> 'display:none']) !!}
				</label>		
				
				<img id="image_show" class="img-responsive" width="400px"  />
			</div>
		</div> 
		 <div class="col-md-pull-6 col-md-6">

			<div class="form-group">
				{!! Form::label('username', 'Nombre del Usuario') !!}
				{!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Nombre del usuario', 'required']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('email', 'Correo Electronico') !!}
				{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Correo Electronico', 'required']) !!}
			</div>

			
			<div class="form-group">
				{!! Form::label('password', 'Clave de ingreso') !!}
				{!! Form::password('password', ['class' => 'form-control', 'required']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('password_confirmation', 'Retipee la clave de ingreso') !!}
				{!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}
			</div>
			
		
			
			<div class="form-group">
				{!! Form::label('countryid', 'Pais') !!}
				{!! Form::select('countryid', $country , null, ['class' => 'form-control', 'data-changeinit']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('cityid', 'Ciudad/Provincia') !!}
				{!! Form::select('cityid', [], null, ['class' => 'form-control']) !!}
			</div>
						
		</div>
		
		<div class="col-md-12 text-center">
			<hr>		
			Si usted desea registrarse como abogado, por favor cambie el estado en el siguiente boton:<br/><br/>
			<div class="form-group">				
				{!! Form::checkbox('is_lawyer', 1, old('is_lawyer'), ['id' => 'is_lawyer' , 'data-toggle' => 'toggle', 'data-on' => '<i class="fa fa-university"></i> Abogado', 'data-off' => '<i class="fa fa-user"></i> Usuario', 'data-onstyle' => 'success']) !!}
			</div>
		</div>
		
		<div class="col-md-12">

			<div class="row">
			  <div class="is_lawyer_box">
				   <div class="col-md-6">
				   	<div class="panel panel-default">
						<div class="panel-heading text-center">
							<strong>Datos Adicionales</strong>
						</div>
						<div class="panel-body">
							<div class="form-group">
								{!! Form::label('name', 'Nombres') !!}
								{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombres']) !!}
							</div>
							
							<div class="form-group">
								{!! Form::label('lastname', 'Apellidos') !!}
								{!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Apellidos']) !!}
							</div>		

							<div class="form-group">
								{!! Form::label('phone', 'Telefono') !!}
								{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Telefono']) !!}
							</div>		

							<div class="form-group">
								{!! Form::label('profesionid', 'Profesión') !!}
								{!! Form::select('profesionid', $profesion , null, ['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								{!! Form::label('postalcode', 'Codigo Postal') !!}
								{!! Form::text('postalcode', null, ['class' => 'form-control', 'placeholder' => 'Codigo Postal']) !!}
							</div>		

							<div class="form-group">
								{!! Form::label('address', 'Direccion Actual') !!}
								{!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Direccion Actual']) !!}
							</div>		
							
							<div class="form-group">
								{!! Form::label('resume', 'Resumen Curricular') !!}
								{!! Form::textarea('resume', null, ['class' => 'form-control textarea-content', 'maxlength' => '1000']) !!}
							</div>
						 </div>
					 </div>
				  </div> 
				  <div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading text-center">
							<strong>Servicios y Detalles de Pago</strong>
						</div>
						<div class="panel-body">
							<div class="form-group">
								{!! Form::label('experience', 'Años de Experiencia') !!}
								{!! Form::text('experience', null, ['class' => 'form-control', 'maxlength' => '50']) !!}
							</div>	
							
							<div class="form-group">
								{!! Form::label('partnership', '¿Has tenido reconocimientos?') !!}
								{!! Form::textarea('partnership', null, ['class' => 'form-control', 'maxlength' => '500', 'rows' => '3', 'style' => 'resize:vertical']) !!}
							</div>
							
							<div class="form-group">							
								{!! Form::label('special', '¿En cuales areas te especializas?') !!}
								<br>
								@foreach($specialities as $special)
									<span class='text-nowrap'>{!! Form::checkbox('special[]', $special->codeid, null) !!} {!! $special->value !!} </span>
								@endforeach								
							</div>
							
							<div class="form-group">							
								{!! Form::label('client', '¿Cuales son tus principales clientes?') !!}
								<br>
								@foreach($client as $cl)
									{!! Form::checkbox('client[]', $cl->codeid, null) !!} {{ $cl->value }}
								@endforeach								
							</div>
							
							<div class="form-group">							
								{!! Form::label('language', 'Idiomas que maneja') !!}
								<br>
								@foreach($language as $lang)
									{!! Form::checkbox('language[]', $lang->codeid, null) !!} {{ $lang->value }}
								@endforeach								
							</div>
							
							<div class="form-group">
								{!! Form::label('service', '¿Que tipo de servicios ofrece?') !!}
								{!! Form::textarea('service', null, ['class' => 'form-control', 'maxlength' => '500', 'rows' => '3', 'style' => 'resize:vertical']) !!}
							</div>
							
							<div class="form-group">							
								{!! Form::label('offer_service', '¿Como ofrece el servicio?') !!}
								<br>
								@foreach($service as $srv)
									{!! Form::checkbox('offer_service[]', $srv->codeid, null) !!} {{ $srv->value }}
								@endforeach								
							</div>
							
							<div class="form-group">
								{!! Form::label('schedule', '¿Cual es tu horario?') !!}
								{!! Form::text('schedule', null, ['class' => 'form-control', 'maxlength' => '50']) !!}
							</div>
							
							<div class="form-group">							
								{!! Form::label('fare', '¿Como son tus tarifas?') !!}
								<br>
								@foreach($fare as $fr)
									{!! Form::checkbox('fare[]', $fr->codeid, null) !!} {{ $fr->value }}
								@endforeach								
							</div>
							
							<div class="form-group">							
								{!! Form::label('fare', '¿Ofrece facilidad en la forma de pago?') !!}
								<br>
								@foreach($payment_mode as $pm)
									{!! Form::checkbox('payment_mode[]', $pm->codeid, null) !!} {{ $pm->value }}
								@endforeach								
							</div>
							
							<div class="form-group">							
								{!! Form::label('payment_type', '¿Cual es su modalidad de pago?') !!}
								<br>
								@foreach($payment_type as $pt)
									{!! Form::checkbox('payment_type[]', $pt->codeid, null) !!} {{ $pt->value }}
								@endforeach								
							</div>
						</div>
					</div>
				 </div>
			 	</div>
			</div>
		</div>
		
		<div class="col-md-12 text-center">			
			<div class="form-group">
				{!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
				&nbsp;
				<a class="btn btn-primary" href="{{ route('index') }}" >Cancelar</a>
			</div>			
		</div>
		 
        </div>
		
		{!! Form::close() !!}
        <!-- /.row -->
		
    </div>
    <!-- /.container -->
	 
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@endsection