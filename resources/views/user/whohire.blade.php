@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad
                    <small>Eventos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
			<div class="col-md-9">
				 <div class="row">				   
						<div class="col-md-12">
							@if($hires)
							<table class="table table-responsive table-striped">
							   <tbody>
							   @foreach($hires as $hire)
								  <tr>
								    <td class="text-nowrap"><i class="fa fa-balance-scale fa-lg" style="color:lightblue;text-shadow: 1px 1px 1px gray;"></i></td>
									<td class="text-nowrap">{{ $hire->user->username }}</td>
									<td class="text-nowrap"><a href="mailto:{{$hire->user->email}}?Subject=Contacto%20de%20Abogado" >{{ $hire->user->email }}</a></td>
									<td class="text-nowrap"><i class="fa fa-clock-o"></i><small> {{ $hire->created_at->diffForHumans() }}</small> </td>
									<td><small>{{ $hire->message }}</small></td>
								  </tr>
								@endforeach
							   </tbody>
							</table>
							{{ $hires->links() }}
							@endif
						</div>
				</div>
			</div>
        </div>
        <!-- /.row -->

    </div>


@endsection