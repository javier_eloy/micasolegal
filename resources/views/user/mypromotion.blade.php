@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad
                    <small>Eventos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
			<!-- Posted Articles -->
			<div class="col-md-9">
			<a class="btn btn-primary" href="{{ route('promotion.create') }}"><i class="fa fa-file-text-o fa-fw"></i>Nueva Promoción</a>
			<hr>
			
				 <div class="row">				   
					<div class="col-md-12">
						<table class="table table-responsive table-striped">
						   <tbody>
						   @foreach($promotions as $promotion)
						      <tr>
								<td><i class="fa fa-gift fa-lg" style="color:lightblue;text-shadow: 1px 1px 1px gray;"></i></td>
								<td><a href="{{ route('promotion.edit',$promotion->promotionid) }}">{{ $promotion->name }}</a></td>
								<td class="text-nowrap"><i class="fa fa-clock-o"></i><small> {{ $promotion->created_at->diffForHumans() }}</small> </td>
								<td class="text-nowrap"><small>{{ $promotion->start_date }} al {{ $promotion->end_date }}</small></td>
							  </tr>
							@endforeach
						   </tbody>
						</table>
				    </div>
				 </div>
				 <hr>
			</div>			
			{{ $promotions->render() }}
        </div>
        <!-- /.row -->

    </div>


@endsection