@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad
                    <small>Eventos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
			<!-- Posted Articles -->
			<div class="col-md-9">
			 <div class="row">				   
					<div class="col-md-12">
						<table class="table table-responsive table-striped">
						   <tbody>
						   @foreach($questions as $question)
						      <tr>
								<td class="text-nowrap"><i class="fa fa-question fa-lg" style="color:lightblue;text-shadow: 1px 1px 1px gray;"></i></td>
								<td><a href="{{ route('question.show',$question->articleid) }}">{{ $question->title }}</a></td>
								<td class="text-nowrap"><i class="fa fa-clock-o"></i><small> {{ $question->created_at->diffForHumans() }}</small> </td>
							  </tr>
							@endforeach
						   </tbody>
						</table>
				    </div>
			</div>
 		    <hr>			
			</div>			
			{{ $questions->render() }}
        </div>
        <!-- /.row -->

    </div>


@endsection