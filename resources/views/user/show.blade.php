@extends('template.main')

 
@section('content')

	<div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mi Actividad</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a>
                    </li>
                    <li class="active">Mi Actividad</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            @include('template.partials.sidebar')
            <!-- Content Column -->
            <div class="col-md-9">
			
                <div class="panel panel-default">
                  <div class="panel-heading"><h4>Estadisticas</h4></div>
                  <div class="panel-body">
                    
                    <small>Preguntas ({{ $CountMyArticles }}/{{ $CountArticles }})</small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $CountMyArticles }}" aria-valuemin="0" aria-valuemax="{{ $CountArticles }}" style="width: {{ ($CountMyArticles > 0) ? ($CountMyArticles*100) / $CountArticles : 0 }}%">
                        <span class="sr-only">{{ $CountMyArticles }} / {{ $CountArticles }}</span>
                      </div>
                    </div>
                    <small>Respuestas ({{ $CountMyComment}}/{{ $CountComment }})</small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $CountMyComment }}" aria-valuemin="0" aria-valuemax="{{ $CountComment }}" style="width: {{ ($CountComment > 0) ? ($CountMyComment * 100) / $CountComment : 0 }}%">
                        <span class="sr-only">{{ $CountMyComment}} / {{ $CountComment }} </span>
                      </div>
                    </div>

                  </div><!--/panel-body-->
              </div><!--/panel-->
			  
            </div>
        </div>
        <!-- /.row -->

    </div>
	 
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
@endsection