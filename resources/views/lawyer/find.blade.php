@extends('template.main')

 
@section('content')
   <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Buscar Abogado</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('lawyer.index') }}">Abogados</a></li>
                    <li class="active">Buscar Abogado</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
			<div class="col-md-3">  
			
			{{ Form::open(['method'=>'post','url' => route('lawyer.search'),'id' => 'search_lawyer' ]) }}
			
				<div class="form-group">
					{!! Form::label('countryid', 'Pais') !!}
					{!! Form::select('countryid', $country , $countryid, ['class' => 'form-control', 'data-changeinit']) !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('cityid', 'Ciudad/Provincia') !!}
					{!! Form::select('cityid', [], null, ['class' => 'form-control']) !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('profesionid', 'Profesión') !!}
					{!! Form::select('profesionid', $profesion , 0, ['class' => 'form-control']) !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('special', 'Especialidad') !!}
					{!! Form::select('specialid', $specialities , $specialid, ['class' => 'form-control']) !!}
				</div>

				{{Form::submit('Buscar', array('name'=>'submit', 'class'=>'btn btn-primary', 'id' => 'find_button'))}}
				{{ Form::close() }}
			</div>
			
			<div class="col-md-9" data-result >  
			
			
			</div>
        </div>

        <!-- /.row -->

    </div>
    <!-- /.container -->

  

@endsection


@section('js')   
	<script src="{{ asset('js/custom.js') }}"></script>
	@if(isset($specialid) && $specialid > 0)
		<script>
		  $(document).ready(function(){
				$("#find_button").submit();
		  });
		</script>
	@endif
@endsection