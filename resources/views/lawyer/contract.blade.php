@extends('template.main')

 
@section('content')
   <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Contratar</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('lawyer.index') }}">Abogados</a></li>
                    <li class="active">{{ $lawyer->lawyer->name." ".$lawyer->lawyer->lastname }}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
			<!-- Contact Form -->
			 <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
			<div class="col-md-8">
			
			  <div class="row">
			    <div class="col-md-2">
				  <img class="img-circle" src="{!! ($lawyer->image_path) ? asset('images/user/'.$lawyer->image_path) :  asset('/images/default-user.png') !!}"  alt=" Abogado " width="100px" height="100px">
				</div>
				<div class="col-md-10 text-center">
				    <h2 class="text-uppercase">{{ $lawyer->lawyer->name."  ". $lawyer->lawyer->lastname}}</h2>
					<p>{{ $lawyer->lawyer->profesion->name }}</p>
					<p><input id="rating-system" type="number" class="rating" min="0" value="{{$lawyer->lawyer->rate}}" max="5" step="1" data-display-only="true" data-stars="5" data-size="xs"></p>
				</div>
			  </div>
			  
			  <div class="row">
			     <div class="col-md-12">
				   <hr>
				   <h5><strong>Resumen Curricular</strong></h5>
				   <hr>
				   <div class="jrm-truncate" data-lines = "6" data-link-caption= "Ver más"  data-link-closecaption = "<< Menos">
					  <div>{{ $lawyer->lawyer->resume }}</div>
				   </div>
				   <hr>
   				   <h5><strong>Servicios y Detalles de Pago</strong></h5>
				   <hr>

					<div class="row"> 
					  @if(!empty($lawyer->lawyer->experience))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Años de Experiencia:</strong>
						  </div>
						  <div class="col-md-9">
						   {{ $lawyer->lawyer->experience }}
						  </div>
					 @endif
					 
					 @if(!empty($lawyer->lawyer->partnership))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Reconocimientos:</strong>
						  </div>
						  <div class="col-md-9">
						   {{ $lawyer->lawyer->partnership }}
						  </div>
					 @endif
					 
					 @if(!empty($lawyer_special))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Especialidades:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_special as $special)
								<span class="text-nowrap"><i class="fa fa-spinner fa-spin"></i> {{ $special }}</span>
							@endforeach
						  </div>					 
					 @endif 
					 
					 @if(!empty($lawyer_client))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Principales Clientes:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_client as $client)
								<i class="fa fa-spinner fa-spin"></i> {{ $client }}
							@endforeach
						  </div>					 
					 @endif 

					 @if(!empty($lawyer_language))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Idiomas que maneja:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_language as $language)
								<i class="fa fa-spinner fa-spin"></i> {{$language}}
							@endforeach
						  </div>					 
					 @endif 
					 
					 @if(!empty($lawyer->lawyer->service))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Servicios que ofrece:</strong>
						  </div>
						  <div class="col-md-9">
						   {{ $lawyer->lawyer->service }}
						  </div>
					 @endif					 
					 
					 @if(!empty($lawyer_offer_service))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Medios del servicio:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_offer_service  as $offer_service)
								<i class="fa fa-spinner fa-spin"></i> {{ $offer_service }}
							@endforeach
						  </div>					 
					 @endif 
					 
					 
					 @if(!empty($lawyer->lawyer->schedule))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Horario de Atención:</strong>
						  </div>
						  <div class="col-md-9">
						   {{ $lawyer->lawyer->schedule }}
						  </div>
					 @endif	
					 
					  @if(!empty($lawyer_fare))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Tipos de tarifas:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_fare  as $fare)
								<i class="fa fa-spinner fa-spin"></i> {{ $fare }}
							@endforeach
						  </div>					 
					  @endif 
					 
					  @if(!empty($lawyer_payment_mode))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Formas de pagos:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_payment_mode  as $payment_mode)
								<i class="fa fa-spinner fa-spin"></i> {{ $payment_mode }}
							@endforeach
						  </div>					 
					  @endif 
					  
					  @if(!empty($lawyer_payment_type))
						  <div class="col-md-3 text-right">
						   <strong class="text-muted">Modalidad de Pagos:</strong>
						  </div>
						  <div class="col-md-9">
						    @foreach( $lawyer_payment_type  as $payment_type)
							   <i class="fa fa-spinner fa-spin"></i> {{ $payment_type }}
							@endforeach
						  </div>					 
					  @endif 
					  
					</div>				   
				   <hr>
				   <div class="row">
					  <div class="col-md-12">
					   <h5><strong>Breve resumen de su caso</strong></h5>
					   <hr>
						{!! Form::open(array('route' => array('lawyer.hire',$lawyer->lawyer->lawyerid), 'class' => 'form')) !!}
						{!! Form::textarea('message', null, array('required', 'class'=>'form-control', 'placeholder'=>'Escriba un resumen de su caso')) !!}					
						<div id="success"></div>
						<br>
						<div class="text-center">
							<div class="small">Al realizar la <mark>Contratar</mark> esta aceptando los terminos del sitio.</div>
							<br>
							<button type="submit" class="btn btn-primary">Contratar</button>
							<a class="btn btn-primary" href="{{ route('lawyer.index') }}">Volver al Directorio</a>
						</div>
						{!! Form::close() !!}
					  </div>
					</div>
				</div>
			   </div>
			</div>
			
			<div class="col-md-4" >
			   <div class="row">
			    
				  @foreach($promotions as $promotion)
					<div class="col-md-12 ">	   
						<div class="media">
								<div class="pull-left">
									<span class="fa-stack fa-2x">																	
										  <div class="avatar-circle text-primary">
											<span class="initials">{{ $promotion->name[0] }}</span>
										  </div>
									</span> 
								</div>
								<div class="media-body">
									<div>							    
										<div data-readmore-title><h5 class="media-heading text-uppercase" ><strong>{!! $promotion->name !!}</strong></h5></div>
										<div class="text-muted small">Promocion valida del {{ $promotion->start_date }} al {{ $promotion->end_date }}</div>
										<div>
										  <span class="label label-danger"> @if($promotion->discount == 0) PROMOCION @else {{ $promotion->discount }}% @endif </span>
										</div>							
									</div>
									<div class="jrm-truncate" data-lines = "5" data-link-caption= "Mas >>"  data-link-closecaption = "<< Menos">
										<span>{!! $promotion->content !!}</span>
									</div>	
								</div>
						</div>
					<br>				  
				  </div>
				@endforeach	
			  </div>			
  		  </div>
			
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

  

@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
@endsection