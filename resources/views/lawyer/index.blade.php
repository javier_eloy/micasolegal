@extends('template.main')

 
@section('content')
   <!-- Page Content -->
    <div class="container">      
		<div role="main" class="main">
			<div class="container">
				<div class="row pt-xl">

					<div class="col-md-9">

						<h1 class="mt-xl mb-none">Abogados</h1>
						<div class="divider divider-primary divider-small mb-xl">
							<hr>
						</div>

						<div class="row">

							<ul class="team-list mt-xs">
							@foreach($lawyers as $lawyer)
								<li class="col-md-4 col-sm-6 mb-xl center isotope-item criminal-law new-york">
									<a href="demo-law-firm-attorneys-detail.html">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
											<span class="thumb-info-wrapper" >
												<img src="{!! ($lawyer->login->image_path) ? asset('images/user/'.$lawyer->login->image_path) :  asset('/images/default-user.png') !!}" class="img-responsive" alt="" style="height:230px">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">Ver mas...</span>
												</span>
											</span>
										</span>
									</a>
									<input id="rating-system" type="number" class="rating" min="0" value="{{$lawyer->rate}}" max="5" step="1" data-display-only="true" data-stars="5" data-size="xs">
									<h4 class="mt-md mb-none">{{ $lawyer->name.' '.$lawyer->lastname }}</h4>
									<p class="mb-none">{{ $lawyer->profesion->name }}</p>
									<span class="thumb-info-social-icons mt-sm pb-none">
										<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
										<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
										<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
									</p>
								</li>
							@endforeach
							</ul>
							{!! $lawyers->render() !!}
						</div>

					</div>

					<div class="col-md-3">
						<aside class="sidebar">
							<div id="combinationFilters" class="filters">

								<h4 class="mt-xl mb-md">Practice Area:</h4>
								<ul class="nav nav-list mb-xlg sort-source team-filter-group" data-filter-group="group1">
									<li data-option-value="" class="active"><a href="#">Show All</a></li>
									<li data-option-value=".criminal-law"><a href="#">Criminal Law</a></li>
									<li data-option-value=".business-law"><a href="#">Business Law</a></li>
									<li data-option-value=".divorce-law"><a href="#">Divorce Law</a></li>
									<li data-option-value=".health-law"><a href="#">Health Law</a></li>
									<li data-option-value=".capital-law"><a href="#">Capital Law</a></li>
								</ul>

								<h4 class="mt-xl mb-md">Location:</h4>
								<ul class="nav nav-list mb-xlg sort-source team-filter-group" data-filter-group="group2">
									<li data-option-value="" class="active"><a href="#">Show All</a></li>
									<li data-option-value=".new-york"><a href="#">New York</a></li>
									<li data-option-value=".london"><a href="#">London</a></li>
								</ul>

							</div>

							<h4 class="mt-xl mb-md"> <a href="{{ route('contact.index')}}">Contactanos</a></h4>
							<div class="divider divider-primary divider-small mb-xl">							    
								<hr>
							</div>

						</aside>
					</div>
				</div>

			</div>
		</div>
     
     </div>        
    <!-- /.container -->


@endsection