@foreach($lawyers as $lawyer)
            <div class="col-md-4 col-sm-6">
                <div class="panel panel-default text-center">
					<img class="img-responsive" src="{!! ($lawyer->login->image_path) ? asset("images/user/".$lawyer->login->image_path) :  asset("/images/default-user.png") !!}"  alt=" Abogado ">
                    <div class="panel-body">
						<a href="{{ route('lawyer.contract', $lawyer->login->loginid) }}">
                          <h4 class="fixed-panel-40">{{ $lawyer->name.' '.$lawyer->lastname }}</h4>
						</a>
						<small>{{ $lawyer->profesion->name }}</small>
                        <p class="fixed-panel">{{ $lawyer->shortresume }}</p>
                        <a href="{{ route('lawyer.contract', $lawyer->login->loginid) }}" class="btn btn-primary">Contratar</a>
                    </div>
                </div>
            </div>
@endforeach
<br>
{{ $lawyers->links() }}