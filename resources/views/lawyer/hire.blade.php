@extends('template.main')

 
@section('content')
   <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Contrato</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('lawyer.index') }}">Abogados</a></li>
                    <li class="active">Contrato</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
			
			<div class="col-md-12">
			   <div class="row">
					<div class="col-md-12 text-center">
					     <img class="img-circle" src="{!! ($hire->lawyers->login->image_path) ? asset('images/user/'.$hire->lawyers->login->image_path) :  asset('/images/default-user.png') !!}"  alt=" Abogado " width="100px" height="100px">
						<h2 class="text-uppercase">{{ $hire->lawyers->name."  ". $hire->lawyers->lastname}}</h2>
						<p>{{ $hire->lawyers->profesion->name }}</p>
						<p><i class="fa fa-phone fa-lg" aria-hidden="true"></i> {{ $hire->lawyers->phone }}</p>
					</div>
				</div>
				<hr>	
			 </div>
			  
			<div class="col-md-12">
				{!! Form::open(['route' => 'lawyer.rate', 'method' => 'POST', 'files' => 'true']) !!}
				<div class="text-center">
					<span class="lead">Servicio contratado</span>
					<p>{{ $hire->message }} </p>
					<hr>
				</div>
				<div class="text-left">
					<span class="lead">Valora el servicio:</span><br/><br/>
					{{ Form::hidden('lawyer_id', $hire->lawyer_id) }}
					<label>{!! Form::radio('rate', '0') !!} No me convenció.</label>
					<label>{!! Form::radio('rate', '1') !!} Esperaba mayor atención.</label>
					<label>{!! Form::radio('rate', '2',true) !!} Estoy satisfecho.</label>
					<label>{!! Form::radio('rate', '3') !!} Más de lo esperado.</label>
					<br/><br/>
					<span class="lead">Comentario </span>
				</div>
				<div class="text-center">
				    {!! Form::textarea('message', null, array('required', 'class'=>'form-control resize-vertical', 'placeholder'=>'Escribe tu opinión del servicio', 'maxlength' => '10000', 'data-charcounter')) !!}<br>
					{!! Form::submit('Valorar', ['class' => 'btn btn-success', 'data-toggle' => 'confirmation' , 'data-title' => '¿Está seguro de su valoración?', 'data-placement' => 'bottom', 'data-btn-ok-label' => 'Aceptar' , 'data-btn-cancel-label' => 'Revisar']) !!}
					&nbsp;<a class="btn btn-primary" href="{{ route('lawyer.index') }}">Ir al Directorio</a>
					&nbsp;<a class="btn btn-primary" href="{{ url()->previous() }}">Atras</a>
				</div>
				{!! Form::close() !!}	
            </div>
			
			
           
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

  

@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	
@endsection