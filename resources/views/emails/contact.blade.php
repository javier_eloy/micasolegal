<!DOCTYPE html>
<body>
<h2>Mensaje de Contacto</h2>
<div>Se ha enviado la siguiente información de contacto:</div>
<div> Contacto: {{ $name }} ( {{ $email }} ) </div>
<div> Telefono: {{ $tel }} </div>
<div> Mensaje: {{ $user_message }}</div>
</body>
</html>