@extends('template.main')

 
@section('content')

 <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Consultas en Linea
				@if(Auth::guest())
					<a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresar al sitio para crear nuevas preguntas"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para crear preguntas</a>
				@else
					<a class="btn btn-primary" href="{{ route('question.create') }}"><i class="fa fa-file-text-o fa-fw"></i>Nueva Pregunta</a>
				@endif
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li class="active">Consulta en Linea</li>
					@if(app('request')->input('category'))
						<li class="active"> </li>
					@endif
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

			@foreach($questions as $question)
                <!-- First Question Post -->
				
				<h4><span class="pull-right label label-default">{{ $question->category->name }}</span></h4>
				
                <h3><a href="{{ route('question.show',$question->articleid) }}">{{ $question->title }}</a> <span class="badge">{{ $question->comment->count() }} </span></h3>
			    <p><strong>por {{ $question->login->username }}</strong> <small><i class="fa fa-clock-o"></i> {{ $question->created_at->diffForHumans() }}</small></p>               
               <!-- <hr>
                <a href="blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/900x300" alt="">
                </a> -->
                <p>{!! $question->ShortContent !!}</p>
                <a class="btn btn-primary" href="{{ route('question.show',$question->articleid) }}">Mas...</a>
				
				<hr>
			@endforeach
                                

                {{ $questions->render() }}

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
				{!! Form::open(['route' => 'question.index', 'method' => 'GET', ]) !!}
                <div class="well">
                    <h4>Busqueda de T&eacute;rminos</h4>
                    <div class="input-group">
					    @if(app('request')->input('category'))
							{!! Form::hidden('category',app('request')->input('category')) !!}
						@endif 
						{!! Form::text('title', app('request')->input('title'), ['class' => 'form-control', 'placelholder' => 'Buscar', 'aria-describedby' => 'search']) !!}						
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>
				{!! Form::close() !!}

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categorias</h4>
                    <div class="row">
                        <div class="col-lg-12">
							@foreach($categories as $category)							
							    <div class="col-md-6">
								 <div>
								   @if( app('request')->input('category') == $category->categoryid )
									   <div class="label label-default"><a href="{{ route('question.index' , ['title' => app('request')->input('title')]) }}" class='close pull-left' data-dismiss='alert'>×</a>{{ $category->name }}</div>
									@else
									   <a href="{{ route('question.index' , ['category' => $category->categoryid, 
																			'title' => app('request')->input('title') ]) }}" > {{ $category->name }}</a>										
									@endif
								 </div>
                                </div>								
							@endforeach                              
                        </div>
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Tips</h4>
                    <p>A través de éste medio podras consultar a profesionales en la materia y tener informaci&oacute;n de tu caso. 
					   Podras ver las opiniones de expertos y te servirá para conocer como piensa tu proximo defensor.</p>
                </div>

            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection
