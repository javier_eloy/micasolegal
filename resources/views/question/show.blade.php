@extends('template.main')

 
@section('content')
  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $question->ShortTitle }}
                    <small>por {{ $question->login->username }}</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('question.index') }}">Consulta en Linea</a></li>
                    <li class="active">{!! $question->ShortTitle !!}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->
                <hr>
                <!-- Date/Time -->
				<span class="pull-right label label-default">{{ $question->category->name }}</span> 
                <p><i class="fa fa-clock-o"></i> {{$question->created_at->diffForHumans() }}</p>
				<hr>
                <!-- Post Content -->
                <p class="lead">{{ $question->title }}</p>
                <p>{!! $question->content !!}</p>
				
				<hr>				
                <!-- Blog Comments -->				
				@if(!Auth::guest())
  				 @if(Auth::user()->is_lawyer == 1)

					<!-- Comments Form -->
					{!! Form::open(['route' => ['question.store.comment', $question->articleid], 'method' => 'POST']) !!}
					<div class="well">
						<h4>Comentario:</h4>
						<form role="form">
							<div class="form-group">
								{{ Form::textarea('text', null, ['class' => 'form-control','rows' => '4','maxlength' => '3000']) }}
							</div>
							<button type="submit" class="btn btn-primary">Enviar</button>
						</form>
					</div>
					{!! Form::close() !!}					
					@endif
				@else
					<a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresa al sitio para comentar"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para responder</a>					
				@endif
				<hr>				
                
                <!-- Posted Comments -->
				@foreach($question->comment as $comment)
                <!-- Comment -->
				
                <div class="row">
                    <div class="col-md-2 pull-left">
                        <img class="img-responsive" src="{!! ($comment->login->image_path) ? asset("/images/user/".$comment->login->image_path) : asset("/images/default-user.png") !!}" alt=" Abogado ">
                    </div>
                    <div class="col-md-10">					 
                        <h4 class="media-heading">{{ $comment->login->username }}
                            <small>{{ $question->created_at->diffForHumans() }}</small>
                        </h4>
                        {!! $comment->text !!}
                    </div>
                </div>
				<br>
				@endforeach
                <!-- Comment 
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Start Bootstrap
                            <small>August 25, 2014 at 9:30 PM</small>
                        </h4>
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        <!-- Nested Comment -->
                     <!--   <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="http://placehold.it/64x64" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Nested Start Bootstrap
                                    <small>August 25, 2014 at 9:30 PM</small>
                                </h4>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>
                        <!-- End Nested Comment -->
                  <!--  </div>
                </div> -->

            </div> 

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categorias</h4>
                    <div class="row">
                        @foreach($categories as $category)
							    <div class="col-md-6">
								   <a href="{{ route('question.index' , ['category' => $category->categoryid ]) }}">{{ $category->name }}</a>
                                </div>
						@endforeach   
                    </div>
                    <!-- /.row -->
                </div>               
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection