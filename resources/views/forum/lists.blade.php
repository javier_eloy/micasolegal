@extends('template.main')

 
@section('content')

 <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Comunidad
				@if(Auth::guest())
					<a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresar al sitio para crear nuevos articulos"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para crear temas en la comunidad</a>
				@else
					<a class="btn btn-primary" href="{{ route('forum.create') }}"><i class="fa fa-file-text-o fa-fw"></i>Nuevo Tema de Conversacion</a>
				@endif
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-10">

					<h3>Categoria: {{ $categoryname }}</h3>
					
					@if(count($forums))
						<table class="table table-striped table-bordered">					
						<thead>					
						  <tr>
							<th class="col-xs-10">Titulo</th>
							<th class="col-xs-2 text-center">Respuestas</th>
						  </tr>
						</thead>
						
						<tbody>
						  @foreach($forums as $forum)
						  <tr>
							<td><a href="{{ route('forum.show',$forum->articleid) }}">{{ $forum->title }}</a></td>
							<td class="text-center">{{ $forum->comment->count() }}</td>
						  </tr>
						  @endforeach            
						</tbody>					
					  </table>
					@else
						<hr>
						<h4>No existen temas</h4>
				 	@endif		
				
            </div>
            

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection
