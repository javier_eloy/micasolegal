@extends('template.main')

 
@section('content')
  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $forum->ShortTitle }}
                    <small>por {{ $forum->login->username }}</small>
                </h1>
				<ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('forum.index') }}">Comunidad</a></li>
					<li><a href="{{ route('forum.lists',$forum->categoryid) }}">{{ $forum->category->name }}</a></li>
                    <li class="active">{!! $forum->ShortTitle !!}</li>
                </ol>
                
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">

            <!-- Forum Post Content Column -->
            <div class="col-lg-12">
                <!-- Forum Post -->
                <!-- Date/Time -->
				<span class="pull-right label label-default">{{ $forum->category->name }}</span>
                <p><small><i class="fa fa-clock-o"></i> {{$forum->created_at->diffForHumans() }}</small></p>
                <!-- Post Content -->
                <p class="lead">{{ $forum->title }}</p>
                <p>{!! $forum->content !!}</p>
				
				<hr>
                <!-- Forum Comments -->				
				@if(!Auth::guest())
					<!-- Comments Form -->
					{!! Form::open(['route' => ['forum.store.comment', $forum->articleid], 'method' => 'POST']) !!}
					<div class="well">
						<h4>Comentario:</h4>
						<form role="form">
							<div class="form-group">
								{{ Form::textarea('text', null, ['class' => 'form-control','rows' => '4','maxlength' => '3000']) }}
							</div>
							<button type="submit" class="btn btn-primary">Enviar</button>
						</form>
					</div>
					{!! Form::close() !!}					                
				@else
				  <a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresa al sitio para comentar"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para responder</a>					
				@endif
				<hr>

                <!-- Posted Comments -->
				@foreach($forum->comment as $comment)
                <!-- Comment -->
				
                <div class="row">
                    <div class="col-md-2 pull-left">
                        <img class="img-responsive" src="{!! ($comment->login->image_path) ? asset("/images/user/".$comment->login->image_path) : asset("/images/default-user.png") !!}" alt=" Abogado ">
                    </div>
                    <div class="col-md-10">					 
                        <h4 class="media-heading">{{ $comment->login->username }}
                            <small>{{ $comment->created_at->diffForHumans() }}</small>
                        </h4>
                        {!! $comment->text !!}
                    </div>
                </div>
				<br>
				@endforeach               

            </div> 
           
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection