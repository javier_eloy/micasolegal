@extends('template.main')

 
@section('content')
  <div role="main" class="main">
  <!-- Header Carousel -->
	<div class="slider-container rev_slider_wrapper" style="height: 650px;">
			<div id="revolutionSlider" class="slider rev_slider manual">
				<ul>
					<li data-transition="fade" data-title="Asistencia Legal" data-thumb="{{ asset('images/header1.jpg') }}">

						<img src="{{ asset('images/header1.jpg') }}"  
							alt=""
							data-bgposition="center center" 
							data-bgfit="cover" 
							data-bgrepeat="no-repeat"
							class="rev-slidebg">

						<div class="tp-caption top-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-95"
							data-start="1000"
							style="z-index: 5"
							data-transform_in="y:[-300%];opacity:0;s:500;">CONFIA EN NOSOTROS</div>

						<div class="tp-caption main-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-45"
							data-start="1500"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							style="z-index: 5"
							data-mask_in="x:0px;y:0px;">EQUIPOS EN LEYES</div>

						<div class="tp-caption bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="5"
							data-start="2000"
							style="z-index: 5"
							data-transform_in="y:[100%];opacity:0;s:500;">Consulta con los mejores abogados.</div>

						<a class="tp-caption btn btn-primary btn-lg"
							data-hash
							data-hash-offset="85"
							href="{{ route('lawyer.find') }}"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="80"
							data-start="2500"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							style="z-index: 5"
							data-mask_in="x:0px;y:0px;">Busca tu abogados</a>
						
					</li>
					<li data-transition="fade" data-title="Profesionales del Derecho" data-thumb="{{ asset('images/header2.jpg') }}">

						<img src="{{ asset('images/header2.jpg') }}"  
							alt=""
							data-bgposition="center center" 
							data-bgfit="cover" 
							data-bgrepeat="no-repeat" 
							class="rev-slidebg">

						<div class="tp-caption main-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-205"
							data-start="1000"
							style="z-index: 5; font-size: 40px;"
							data-transform_in="y:[-300%];opacity:0;s:500;">Profesionales del Derecho</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-145"
							data-start="1500"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Abogados Civil</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-100"
							data-start="1800"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Abogados Mercantil</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-55"
							data-start="2100"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Abogado Penitenciario</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="-10"
							data-start="2400"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Divorcios</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="35"
							data-start="2700"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Abogados Gubernamental</div>

						<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="80"
							data-start="3000"
							data-transform_in="x:[-100%];opacity:0;s:500;"><i class="fa fa-check"></i> Abogados en Accidentes</div>

						<a class="tp-caption btn btn-primary btn-lg"
							data-hash
							data-hash-offset="85"
							href="{{ route('lawyer.find') }}"
							data-x="right" data-hoffset="100"
							data-y="center" data-voffset="150"
							data-start="3500"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							style="z-index: 5"
							data-mask_in="x:0px;y:0px;">Ver mas <i class="fa fa-long-arrow-right"></i></a>

					</li>
					<li data-transition="fade" data-title="Información al Dia" data-thumb="{{ asset('images/header3.jpg') }}">

						<img src="{{ asset('images/header3.jpg') }}"  
							alt=""
							data-bgposition="center center" 
							data-bgfit="cover" 
							data-bgrepeat="no-repeat" 
							class="rev-slidebg">

						<div class="tp-caption top-label"
							data-x="left" data-hoffset="100"
							data-y="center" data-voffset="-95"
							data-start="1000"
							style="z-index: 5"
							data-transform_in="y:[-300%];opacity:0;s:500;">Bienvenido a</div>

						<div class="tp-caption main-label"
							data-x="left" data-hoffset="100"
							data-y="center" data-voffset="-45"
							data-start="1500"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							style="z-index: 5"
							data-mask_in="x:0px;y:0px;">Nuestro Sitio</div>

						<div class="tp-caption bottom-label"
							data-x="left" data-hoffset="100"
							data-y="center" data-voffset="5"
							data-start="2000"
							style="z-index: 5"
							data-transform_in="y:[100%];opacity:0;s:500;">Las consultas con el mayor directorio de abogados.</div>

						<a class="tp-caption btn btn-primary btn-lg"
							href="{{ route('question.index') }}"
							data-x="left" data-hoffset="100"
							data-y="center" data-voffset="80"
							data-start="2500"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							style="z-index: 5"
							data-mask_in="x:0px;y:0px;">Consulta en Linea</a>

					</li>
				</ul>
			</div>
		</div>
 

	@if(\Auth::guest() && 1 == 2)
	<section id="register" class="banner " data-spy="affix" data-offset-top="199">
		<button type="button" id="close-banner" class="close" aria-label="Close">
			<span aria-hidden="true">&times;&nbsp;</span>
		</button>
	  <div class="container">
	    <div class="text-center">
		   <h3>Gestiona tu caso o publica tus servicios</h3>
	       <p class="lead"><a href="{{ route('user.create') }}">Registrate</a> para disfrutar los beneficios de estar al dia con orientación legal o  <br> <a href="{{route('auth.login')}}">Ingresa</a> para participar</p>
		</div>
	  </div>
	</section>
	@endif
	
	
	
	<!-- Especialidad-->
	<section  id="speciality" class="section section-default section-no-border mt-none">
		<div class="container">
			<div class="row mb-xl">
			<h2 class="mt-xl mb-none">Especialidad</h2>
					<div class="divider divider-primary divider-small mb-xl">
						<hr>
			</div>
			<ul class="row list-unstyled">	
				@foreach($specialities as $special)						
				 <li class="col-xs-3 col-md-1 text-center"> 
					<a class="special" href="{{ route('lawyer.find',$special->codeid) }}">
						<div><i class="fa fa-2x fa-users {{ $special->additional }}" aria-hidden="true"></i></div>
						<br>
						<div class="small">{{ $special->value }}</div>
					</a>
				</li>
				@endforeach			
				
				</ul>
			</div>
		</div>
	</section>

	
    <!-- Articles -->
	<section class="container" id="articulos">
				
			<div class="row">
			  @if(count($questions))
				<div class="col-md-12 center">
						<h2 class="mt-xl mb-none">Ultimas Consultas en Linea</h2>
						<span class="small"><a href="{{route('question.index') }}">Ver todas</a></span>
						<div class="divider divider-primary divider-small divider-small-center mb-xl">
							<hr>
						</div>
				</div>
				@endif
												
				@foreach($questions as $question)		
				<div class="col-md-4 appear-animation animated fadeInUp appear-animation-visible" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
					<div class="panel fixed-panel-380">
						<div class="panel-heading">
							<a href="{{ route('question.show',$question->articleid) }}" data-toggle="tooltip" title="{{ $question->title}}" data-original-title="{{ $question->title}}"><h4><i class="fa fa-fw fa-gavel fa-flip-horizontal"></i> {!! $question->ShortTitle !!}</h4></a>
							<div class="small text-muted">Por  {{ $question->login->username }}</div>
							<div class="small text-muted"><i class="fa fa-clock-o"></i> {{ $question->created_at->diffForHumans() }} </div>
						</div>
						<div class="panel-body">
							<div><p><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">{!! $question->ShortContent300 !!}</span></p></div>						
							<a class="mt-md" href="{{ route('question.show',$question->articleid) }}">Ver más <i class="fa fa-long-arrow-right"></i></a>
							<small class="pull-right ">{!! $question->comment->count() !!} comentarios</small>
						</div>
					</div>
				</div>
				@endforeach
				
		  </div>
	</section>
	</div>

	
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-6 p-none">
				<section class="section section-primary match-height mt-xl" style="background-image: url({{ asset('images/patterns/fancy.jpg')}} );">
					<div class="row">
						<div class="col-half-section col-half-section-right">
							<h2 class="mb-none">Testimonios</h2>
							<div class="divider divider-light divider-small mb-xl">
								<hr>
							</div>

							<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': false, 'dots': true}">
								<div>
									<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
										<blockquote>
											<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis.</p>
										</blockquote>
										<div class="testimonial-author">
											<div class="testimonial-author-thumbnail">
												<img src="img/clients/client-1.jpg" class="img-responsive img-circle" alt="">
											</div>
											<p><strong>John Smith</strong><span class="text-light">CEO & Founder - Okler</span></p>
										</div>
									</div>
								</div>
								<div>
									<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
										<blockquote>
											<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet.</p>
										</blockquote>
										<div class="testimonial-author">
											<div class="testimonial-author-thumbnail">
												<img src="img/clients/client-2.jpg" class="img-responsive img-circle" alt="">
											</div>
											<p><strong>Jessica Smith</strong><span class="text-light">Marketing - Okler</span></p>
										</div>
									</div>
								</div>
								<div>
									<div class="testimonial testimonial-style-3 testimonial-trasnparent-background testimonial-alternarive-font">
										<blockquote>
											<p class="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis.</p>
										</blockquote>
										<div class="testimonial-author">
											<div class="testimonial-author-thumbnail">
												<img src="img/clients/client-3.jpg" class="img-responsive img-circle" alt="">
											</div>
											<p><strong>Bob Smith</strong><span class="text-light">COO - Okler</span></p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
			<div class="col-md-6 p-none visible-md visible-lg">
				<section class="parallax section section-parallax match-height mt-xl" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="{{ asset('images/parallax-law-firm.jpg') }}" style="min-height: 450px;">
				</section>
			</div>
		</div>
				
		<!-- Lawyers -->
		<div class="container">
			<div class="row">
				<div class="col-md-12 center">
					<h2 class="mt-xl mb-none">Abogados</h2>
					<div class="divider divider-primary divider-small divider-small-center mb-xl">
						<hr>
					</div>
				</div>
			</div>
			<div class="row mt-lg">
				<div class="owl-carousel owl-theme owl-team-custom show-nav-title" data-plugin-options="{'items': 4, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">David Doe</h4>
						<p class="mb-none">Criminal Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
							<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">Jeff Doe</h4>
						<p class="mb-none">Business Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="mailto:mail@example.com"><i class="fa fa-envelope"></i><span>Email</span></a>
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">Craig Doe</h4>
						<p class="mb-none">Divorce Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
							<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">Richard Doe</h4>
						<p class="mb-none">Accident Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">Amanda Doe</h4>
						<p class="mb-none">Health Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="center mb-lg">
						<a href="demo-law-firm-attorneys-detail.html">
							<img src="{{ asset('images/team-22.jpg') }}" class="img-responsive" alt="">
						</a>
						<h4 class="mt-md mb-none">Jessica Doe</h4>
						<p class="mb-none">Capital Law</p>
						<span class="thumb-info-social-icons mt-sm pb-none">
							<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Contadores -->
	<section class="parallax section section-text-light section-parallax section-center mt-xl" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="{{ asset('images/parallax-law-firm-2.jpg') }}">
		<div class="container">
			<div class="row">
				<div class="counters counters-text-light">
					<div class="col-md-3 col-sm-6">
						<div class="counter mb-lg mt-lg">
							<i class="icon-user-following icons"></i>
							<strong data-to="25000" data-append="+">0</strong>
							<label>Happy Clients</label>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="counter mb-lg mt-lg">
							<i class="icon-diamond icons"></i>
							<strong data-to="15">0</strong>
							<label>Years in Business</label>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="counter mb-lg mt-lg">
							<i class="icon-badge icons"></i>
							<strong data-to="3">0</strong>
							<label>Awards</label>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="counter mb-lg mt-lg">
							<i class="icon-paper-plane icons"></i>
							<strong data-to="178">0</strong>
							<label>Successful Stories</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
				
		
	<!-- Fin - contadores -->
	
	
	<div class="container" id="promociones">
		<div class="row">
		   @if(count($promotions))
			<div class="col-md-12 center">
				<h2 class="mt-xl mb-none">Ultimas Promociones</h2>
				<span class="small"><a href="{{route('promotion.index') }}"> Ver todas </a></span>
				<div class="divider divider-primary divider-small divider-small-center mb-xl">
					<hr>
				</div>
			</div>
			@endif
		</div>

		<div class="row mt-lg">
		
			@foreach($promotions as $promotion)
			<div class="col-md-4">
				<div class="feature-box feature-box-style-2 mb-xl appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
					<div class="feature-box-icon">
						 <div class="avatar-circle text-primary">
							<a href="{{ route('lawyer.contract', $promotion->loginid) }}"><span class="initials">{{ $promotion->name[0] }}</span></a>
						  </div>
						<!-- <img src="img/demos/law-firm/icons/criminal-law.png" alt="" /> -->
					</div>
					<div class="feature-box-info ml-md">
						<h4 class="mb-sm">{!! $promotion->shortname !!}</h4>
						<div class="text-muted small">Promocion valida del {{ $promotion->start_date }} al {{ $promotion->end_date }}</div>
						<span class="label label-danger"> @if($promotion->discount == 0) PROMOCION @else {{ $promotion->discount }}% @endif </span>
						<p>{!! $promotion->shortcontent !!}</p>
						<a class="mt-md" href="{{ route('lawyer.contract', $promotion->loginid) }}">Ver mas <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>
			</div>
			@endforeach
		</div>	
	</div>

 
 <section  class="section">
 	<div class="container">
		<div class="row">
		   @if(count($blogs))
			<div class="col-md-12 center">
				<h2 class="mt-xl mb-none">Ultimos Articulos</h2>
				<span class="small"><a href="{{route('blog.index') }}"> Ver todas </a></span>
				<div class="divider divider-primary divider-small divider-small-center mb-xl">
					<hr>
				</div>
			</div>
			@endif
		</div>
		<div class="row mt-xl">
			@foreach($blogs as $blog)
			<!-- First Blog Post -->
			<div class="col-md-6">
				<span class="thumb-info thumb-info-side-image thumb-info-no-zoom mb-xl">
					<span class="thumb-info-side-image-wrapper p-none hidden-xs">
						<a title="" href="{{ route('blog.show',$blog->articleid) }}">
							<img src="{!! ($blog->image_path) ? asset('images/user/'.$blog->image_path) :  asset('/images/default-blog.jpg') !!}" class="img-responsive" alt="" style="width: 195px; height:235px">
						</a>
					</span>
					<span class="thumb-info-caption">
						<span class="thumb-info-caption-text">
							<h2 class="mb-md mt-xs"><a title="" class="text-dark" href="demo-law-firm-news-detail.html">{{ $blog->title }}</a></h2>
							<span class="post-meta">
								<span>{{ $blog->created_at->diffForHumans() }}  | {{ $blog->comment->count() }} comentarios  | <a href="#">John Doe</a></span>
							</span>
							<p class="font-size-md">{!! trim($blog->ShortContent150) !!}</p>
							<a class="mt-md" href="demo-law-firm-news-detail.html">Ver más <i class="fa fa-long-arrow-right"></i></a>
						</span>
					</span>
				</span>

			</div>
			@endforeach
			
		</div>
	</div>
 </section>
 <!-- Link to find lawyer -->
	<div class="fixed-find-lawyer" style="display:none">	
		<a href="{{ route('lawyer.find') }}" >
		 <span class="tags"><span class="post-tag">¿Buscas un Abogado?</span></span>		 
		 <img class="img-circle find-lawyer" src="{{ asset('images/lawyer_find.png') }}"></img>
		 </a>
	</div>
  
@endsection

@section('js')
	

    <!-- Custom function -->
    <script src="{{ asset('js/custom.js') }}"></script>
@endsection