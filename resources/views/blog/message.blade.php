@extends('template.main')

 
@section('content')
  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mensaje</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>                    
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-10">
				
				<h4>{{ $msg }} </h4>

            </div> 
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection