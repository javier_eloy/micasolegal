@extends('template.main')

 
@section('content')

<!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nuevo Articulo
                    <small>Describe tu caso</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('blog.index') }}">Home</a></li>
					<li><a href="{{ route('blog.index') }}">Articulos</a></li>
                    <li class="active">Nuevo Articulo</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-12">
                
			{!! Form::open(['route' => 'blog.store', 'method' => 'POST', 'files' => 'true']) !!}
				
				<div class="form-group">
					{!! Form::label('title', 'Titulo') !!}
					{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titulo del articulo', 'required', 'maxlength' => '200']) !!}
				</div>

				@if($option->has_category == 1)
				<div class="form-group">
					{!! Form::label('categoryid', 'Categoria') !!}
					{!! Form::select('categoryid', $categories, null, ['class' => 'form-control', 'placeholder' => 'Selecciones una categoria', 'required']) !!}
				</div>
				@endif
				
				@if($option->has_section == 1)
				<div class="form-group">
					{!! Form::label('sectionid', 'Seccion') !!}
					{!! Form::select('sectionid', $sections, null, ['class' => 'form-control', 'placeholder' => 'Selecciones una seccion', 'required']) !!}
				</div>
				@endif

				<div class="form-group">
					{!! Form::label('content', 'Contenido') !!}
					{!! Form::textarea('content', null, ['class' => 'form-control textarea-content', 'maxlength' => '15000', 'required']) !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('image', 'Imagen') !!}
					{!! Form::file('image') !!}
				</div>
				
				@if($option->has_tags == 1)
				<div class="form-group">
					{!! Form::label('tags', 'Tags') !!}
					{!! Form::select('tags[]', $tags, null, ['class' => 'form-control select-tag', 'multiple']) !!}
				</div>
				@endif
			
				<div class="form-group">
					{!! Form::submit('Crear', ['class' => 'btn btn-primary']) !!}
				</div>
			{!! Form::close() !!}

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

@endsection

@section('js')
<script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script> 
<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>

<script>
	$('.select-tag').chosen({
		placeholder_text_multiple: 'Seleccione un maximo de 3 tags',
		max_selected_options: 3,
		search_contains: true,
		no_results_text: 'No se encontraron estos tags'
	});

	$('.textarea-content').trumbowyg();
</script>
@endsection