@extends('template.main')

 
@section('content')

 <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row pt-xl mb-lg">
            <div class="col-md-12">
				<h1 class="mt-xl mb-none">Articulos
                @if(Auth::guest())
					<a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresar al sitio para crear nuevos articulos, recuerda que solamente los registrados como abogados podrán usar de éste servicio"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para crear articulos</a>
				@else
					<a class="btn btn-primary" href="{{ route('blog.create') }}"><i class="fa fa-file-text-o fa-fw"></i>Nuevo Articulo</a>
				@endif
                </h1>
				<div class="divider divider-primary divider-small mb-xl">
					<hr>
				</div>
            </div>			
        </div>
        <!-- /.row -->

        <div class="row pt-xl mb-lg">

			@include('template.partials.sectionbar')
			
            <!-- Blog Entries Column -->
            <div class="col-md-9">
				
				<div class="row">
				@foreach($blogs as $blog)
					<!-- First Blog Post -->
					<div class="row mb-lg">
						<div class="col-md-12">

							<span class="thumb-info thumb-info-side-image thumb-info-no-zoom mb-lg">
								<span class="thumb-info-side-image-wrapper p-none hidden-xs">
									<a title="" href="{{ route('blog.show',$blog->articleid) }}">
										<img src="{!! ($blog->image_path) ? asset('images/user/'.$blog->image_path) :  asset('/images/default-blog.jpg') !!}" class="img-responsive" alt="" style="width: 195px; height:235px;">
									</a>
								</span>
								<span class="thumb-info-caption">
									<span class="thumb-info-caption-text">
										<h2 class="mb-md mt-xs"><a title="" class="text-dark" href="{{ route('blog.show',$blog->articleid) }}">{{ $blog->title }}</a></h2>
										<span class="post-meta">
											<span>{{ $blog->created_at->diffForHumans() }} | {{ $blog->comment->count() }} comentarios | <a href="#">John Doe</a></span>
										</span>
										<p class="font-size-md">{!! $blog->ShortContent300 !!}</p>
										<a class="mt-md" href="{{ route('blog.show',$blog->articleid) }}">Leer mas <i class="fa fa-long-arrow-right"></i></a>
									</span>
								</span>
							</span>

						</div>
					</div>
			
				@endforeach
				
				{{ $blogs->render() }}
				</div>
            </div>           

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection
