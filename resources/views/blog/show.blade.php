@extends('template.main')

 
@section('content')
  <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <br>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
					<li><a href="{{ route('blog.index') }}">Articulos</a></li>
                    <li class="active">{!! $blog->ShortTitle !!}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row-fluid">
			<div class="col-md-2 col-md-push-10">
				<label class="label label-warning float-right">{{ $blog->section->name or '(Sin seccion)' }}</label>
			</div>
            <!-- Blog Post Content Column -->
            <div class="col-md-10 col-md-pull-2">

                <div class="row">
					<div class="col-md-12">
						<h1 class="text-capitalize">{{ $blog->title }}</h1>
						<div class="small text-muted">
						   Creado por {{ $blog->login->lawyer->name. " ". $blog->login->lawyer->lastname}} /
						   <i class="fa fa-clock-o"></i> {{$blog->created_at->diffForHumans() }}<br/>
						</div>
					</div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						 <img class="inline-image"  src="{!! ($blog->image_path) ? asset('images/user/'.$blog->image_path) :  asset('/images/default-blog.jpg') !!}" alt="">
						 <div>{!! $blog->content !!}</div>
					</div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						@if(!Auth::guest())
						 @if(Auth::user()->is_lawyer == 1)
							<!-- Comments Form -->
							{!! Form::open(['route' => ['blog.store.comment', $blog->articleid], 'method' => 'POST']) !!}
							<div class="well">
								<h4>Comentario:</h4>
								<form role="form">
									<div class="form-group">
										{{ Form::textarea('text', null, ['class' => 'form-control resize-vertical','rows' => '4','maxlength' => '15000']) }}
									</div>
									<button type="submit" class="btn btn-primary">Enviar</button>
								</form>
							</div>
							{!! Form::close() !!}
							
							@endif
						@else
						<a class="btn btn-primary" href="{{ route('auth.login') }}" title="Debe ingresa al sitio para comentar"><i class="fa fa-sign-in fa-fw"></i>Inicia sesión para responder</a>									
						@endif
					</div>
				</div>
				
                <!-- Posted Comments -->
				@foreach($blog->comment as $comment)
				
                <div class="row">
                    <div class="col-md-2 pull-left">
                        <img class="img-responsive" src="{!! ($comment->login->image_path) ? asset("/images/user/".$comment->login->image_path) : asset("/images/default-user.png") !!}" alt=" Abogado ">
                    </div>
                    <div class="col-md-10">					 
                        <h4> <span class="text-capitalize">{{ $comment->login->username }}</span> <span class="small text-muted">{{ $blog->created_at->diffForHumans() }}</span></h4>
						<div>
							{!! $comment->text !!}
						</div>
                    </div>
                </div>
				<br>
				@endforeach            
			  </div>
			  
			  
		
            
            </div> 
			
			

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->


@endsection