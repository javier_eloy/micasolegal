@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-group"></i>Usuarios
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">
                     <div class="col-lg-6">
                        <h2>Usuarios</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Correo</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                                <tbody>
									@foreach($users as $user )
                                    <tr>
                                        <td><a href="{{ route('admin.profile.edit', $user->loginid) }}">{{ $user->username }}</a></td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->TranslateRole }}</td>
                                    </tr>                                   
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h2>Abogados</h2>
                        <div class="table-responsive">
                             <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Correo</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                                <tbody>
									@foreach($lawyers as $lawyer )
                                    <tr>
                                        <td><a href="{{ route('admin.profile.edit', $lawyer->loginid) }}">{{ $lawyer->username }}</a></td>
                                        <td>{{ $lawyer->email }}</td>
                                        <td>{{ $lawyer->TranslateRole }}</td>
                                    </tr>                                    
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection