@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
							<li>
							    <i class="fa fa-wrench"></i> <a href="{{ route('admin.config') }}">Configuración</a>
							</li>
                            <li class="active">Profesion</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">			
				<div class="col-md-10 col-md-offset-1">
				@if (count($errors) > 0)
					<div class="alert alert-danger alert-dismissable">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				  {!! Form::open(['route' => 'admin.profession.insert', 'method' => 'PUT']) !!}
					<div class="form-group">
						{!! Form::label('name', 'Titulo de la Profesión') !!}
						{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Titulo', 'required', 'maxlength' => '200']) !!}						
                    </div>	
					
					<div class="form-group">
						{!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
					</div>						
				 {!! Form::close() !!}
				</div>
			</div>
			
				 <hr>
			
			<div class="row">	
				 {!! Form::open(['route' => 'admin.profession.destroy', 'method' => 'POST']) !!}
				 	<div class="col-md-12">
					@foreach($professions as $profession )
						<div class="form-group col-md-3">
							<input type="checkbox" name="checked[]" value="{{ $profession->profesionid }}"> <a href="#">{{ $profession->name }}</a>
						</div>
					@endforeach					
					</div>				
					
					<div class="col-md-12">
						<div class="form-group">
						{!! Form::submit('Eliminar', ['class' => 'btn btn-primary', 'data-toggle' => 'confirmation', 'data-title' => '¿Está seguro de eliminar?', 'data-btn-ok-label' => 'Aceptar' , 'data-btn-cancel-label' => 'Cancelar']) !!}
						</div>						
					</div>
				 {!! Form::close() !!}
				 {{ $professions->render() }}				 				
            </div>
				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
@endsection