@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Administración
                            <small>Usuarios</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-group"></i>Usuarios
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
                  {!! Form::open(['route' => ['admin.profile.update', $profile], 'method' => 'PUT']) !!}
					<div class="form-group">
						{!! Form::label('username', 'Usuario') !!}
						{!! Form::text('username', $profile->username, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required', 'maxlength' => '200']) !!}
                    </div>
					
					<div class="form-group">
						{!! Form::label('email', 'Correo') !!}
						{!! Form::text('email', $profile->email, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required', 'maxlength' => '200']) !!}
                    </div>

					<div class="form-group">
						{!! Form::label('level', 'Nivel') !!}
					    {!! Form::select('level', $levels, $profile->level, ['class' => 'form-control', 'placeholder' => 'Selecciones una opcion', 'required']) !!}
                    </div>
					
					<div class="form-group">
						{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
					</div>				  
				  {!! Form::close() !!}
				 </div>
            </div>
				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection