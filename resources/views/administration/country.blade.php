@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i> <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
							<li>
							    <i class="fa fa-wrench"></i> <a href="{{ route('admin.config') }}">Configuración</a>
							</li>
                            <li class="active">Paises</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">			
				<div class="col-md-10 col-md-offset-1">
					 <div class="form-group">
						@if (count($errors) > 0)
							<div class="alert alert-danger alert-dismissable">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					
				    {!! Form::open(['route' => 'admin.country.insert', 'method' => 'PUT']) !!}
					<div class="form-group">
						<div class="col-md-9">
							{!! Form::label('name', 'Nombre del Pais') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required', 'maxlength' => '200']) !!}						
						</div>
						
						<div class="col-md-3">						
						    {!! Form::label('initial', 'Iniciales (ISO Alpha-2)') !!} <a href="http://www.nationsonline.org/oneworld/country_code_list.htm" class="text-nowrap" target="_blank" title="Muestra la lista de codigos Alpha-2 para los paises, necesario para ubicar busquedas en su pais"><sup>Ver</sup></a>
							{!! Form::text('initial', null, ['class' => 'form-control', 'placeholder' => 'Iniciales', 'required', 'maxlength' => '3', 'title' => 'Iniciales (ISO Alpha-2)']) !!}							
							<br>
						</div>
                    </div>	
					
					<div class="form-group">
					   <div class="col-md-12">
						  {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
						</div>
					</div>						
				 {!! Form::close() !!}
				</div>
			</div>
			
				 <hr>
			
			<div class="row">	
				 {!! Form::open(['route' => 'admin.country.destroy', 'method' => 'POST']) !!}
				 	<div class="col-md-12">
					@foreach($countrys as $country )
						<div class="form-group col-md-3">
							<input type="checkbox" name="checked[]" value="{{ $country->countryid }}"> <a href="#">{{ $country->name }} ({{ $country->initial }})</a>
						</div>
					@endforeach					
					</div>				
					
					<div class="col-md-12">
						<div class="form-group">
						{!! Form::submit('Eliminar', ['class' => 'btn btn-primary', 'data-toggle' => 'confirmation', 'data-title' => '¿Está seguro de eliminar?', 'data-btn-ok-label' => 'Aceptar' , 'data-btn-cancel-label' => 'Cancelar']) !!}
						</div>						
					</div>
				 {!! Form::close() !!}
				 {{ $countrys->render() }}				 				
            </div>
				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
@endsection