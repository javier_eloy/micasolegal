@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-wrench"></i> Configuracion
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">
                     <div class="col-lg-12">
                        <h2>Tablas</h2>
                            <table class="table table-hover table-striped table-responsive">
							    <thead>
								   <th style="width:90%"></th>
								   <th></th>
								</thead>								
                                <tbody>
                                    <tr>
                                        <td>Paises</td>
										<td><a class="btn btn-warning" href="{{route('admin.country')}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                    </tr>                                   
									<tr>
                                        <td>Ciudades</td>
										<td><a class="btn btn-warning" href="{{ route('admin.city') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                    </tr>                                   
                                    <tr>
                                        <td>Profesión</td>
										<td><a class="btn btn-warning" href="{{ route('admin.profession') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                                    </tr>   
                                </tbody>
								
                            </table>
                    </div>                    
                </div>				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection