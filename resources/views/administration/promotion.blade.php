@extends('template.admin')

 
@section('content')


     <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="{{ route('admin.index') }}">Administracion</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Promociones
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
			<div class="row">
                     <div class="col-lg-12">
                            <table class="table table-hover table-striped">
								<thead>
								   <tr>
								     <th class="text-left"> Nombre </th>
									 <th class="text-center"> Costo </th>
									 <th class="text-center"> Permite Crear Articulos </th>
									 <th class="text-center"> Numero de Promociones Maxima</th>
									 <th></th>
								   </tr>
								</thead>
                                <tbody>
									@foreach($promotions As $promotion)
                                    <tr>
                                        <td class="text-left text-uppercase">{{ $promotion->name }}</td>
										<td class="text-center">{{ $promotion->cost }}</td>
										<td class="text-center"> {{ ($promotion->allow_article == 1) ?  "Si" : "No"  }}</td>
										<td class="text-center"> {{ $promotion->n_promo }} </td>
										<td><a href="" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                    </tr>                                   
									@endforeach
									
                                </tbody>
								
                            </table>
                    </div>                    
                </div>				

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->


@endsection