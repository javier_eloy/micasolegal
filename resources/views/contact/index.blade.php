@extends('template.main')

 
@section('content')

	<div role="main" class="main">
		<div class="container">
			<div class="row pt-xl">
				<div class="col-md-7">
					{!! Form::open(array('route' => 'contact.send', 'class' => 'form')) !!}
					<h1 class="mt-xl mb-none">Contactanos</h1>
					<div class="divider divider-primary divider-small mb-xl">
						<hr>
					</div>
					<p class="lead mb-xl mt-lg">Envia tus comentarios.</p>

					<div class="alert alert-success hidden mt-lg" id="contactSuccess">
						<strong>Success!</strong> Su mensaje ha sido enviado.
					</div>

					<div class="alert alert-danger hidden mt-lg" id="contactError">
						<strong>Error!</strong> There was an error sending your message.
						<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
					</div>

					<form id="contactForm" action="php/contact-form.php" method="POST">
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									{!! Form::text('name', null, ['required', 'class'=>'form-control input-lg', 'placeholder'=>'Ingrese su nombre', 'data-msg-required' => 'Por favor, ingrese su nombre', 'maxlength' => '100']) !!}                            
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
								    {!! Form::text('tel', null, ['required', 'class'=>'form-control input-lg','placeholder'=>'Su telefono', 'data-msg-required' => 'Por favor, ingrese el telefono', 'maxlength' => '30']) !!}

								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									{!! Form::text('email', null, ['required', 'class'=>'form-control input-lg', 'placeholder'=>'Dirección de Correo', 'data-msg-required' => 'Por favor, ingrese la dirección de correo']) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
								   {!! Form::textarea('message', null, array('required', 'class'=>'form-control input-lg', 'placeholder'=>'Escriba los motivos para contactarnos', 'data-msg-required' => 'Por favor, ingrese el mensaje')) !!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">Enviar</button>
							</div>
						</div>
					</form>
					{!! Form::close() !!}
				</div>
				
				<div class="col-md-4 col-md-offset-1">

					<h4 class="mt-xl mb-none">Nuestra Oficina</h4>
					<div class="divider divider-primary divider-small mb-xl">
						<hr>
					</div>

					<ul class="list list-icons list-icons-style-3 mt-xlg mb-xlg">
						<li><i class="fa fa-map-marker"></i> <strong>Dirección:</strong> 1234 Street Name, City Name</li>
						<li><i class="fa fa-phone"></i> <strong>Telefono:</strong> (123) 456-789</li>
						<li><i class="fa fa-envelope"></i> <strong>Correo:</strong> <a href="mailto:mail@example.com">contacto@tepublico.es</a></li>
					</ul>

					<h4 class="pt-xl mb-none">Horario de Trabajo</h4>
					<div class="divider divider-primary divider-small mb-xl">
						<hr>
					</div>

					<ul class="list list-icons list-dark mt-xlg">
						<li><i class="fa fa-clock-o"></i> Lunes - Viernes - 9am to 5pm</li>
						<li><i class="fa fa-clock-o"></i> Sabado - 9am to 2pm</li>
						<li><i class="fa fa-clock-o"></i> Domingo - Closed</li>
					</ul>

				</div>
			</div>
		</div>

		<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
		<div id="googlemaps" class="google-map google-map-footer"></div>
	</div>
	

@endsection

@section('js')
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
  <script src="{{ asset('plugins/jquery.gmap/jquery.gmap.min.js') }}"></script>
  <script>

		/*
		Map Settings

			Find the Latitude and Longitude of your address:
				- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
				- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

		*/

		// Map Markers
		var mapMarkers = [{
			address: "New York, NY 10017",
			html: "<strong>New York Office</strong><br>New York, NY 10017",
			icon: {
				image: "img/pin.png",
				iconsize: [26, 46],
				iconanchor: [12, 46]
			},
			popup: true
		}];

		// Map Initial Location
		var initLatitude = 40.75198;
		var initLongitude = -73.96978;

		// Map Extended Settings
		var mapSettings = {
			controls: {
				draggable: (($.browser.mobile) ? false : true),
				panControl: true,
				zoomControl: true,
				mapTypeControl: true,
				scaleControl: true,
				streetViewControl: true,
				overviewMapControl: true
			},
			scrollwheel: false,
			markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
			zoom: 16
		};

		var map = $('#googlemaps').gMap(mapSettings),
			mapRef = $('#googlemaps').data('gMap.reference');

		// Map Center At
		var mapCenterAt = function(options, e) {
			e.preventDefault();
			$('#googlemaps').gMap("centerAt", options);
		}

		// Create an array of styles.
		var mapColor = "#cfa968";

		var styles = [{
			stylers: [{
				hue: mapColor
			}]
		}, {
			featureType: "road",
			elementType: "geometry",
			stylers: [{
				lightness: 0
			}, {
				visibility: "simplified"
			}]
		}, {
			featureType: "road",
			elementType: "labels",
			stylers: [{
				visibility: "off"
			}]
		}];

		var styledMap = new google.maps.StyledMapType(styles, {
			name: 'Styled Map'
		});

		mapRef.mapTypes.set('map_style', styledMap);
		mapRef.setMapTypeId('map_style');

	</script>

@endsection