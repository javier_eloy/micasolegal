@extends('template.main')

 
@section('content')
  <!-- Page Content -->
    <div class="container">
	
	   <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Promociones</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ route('index')}}">Home</a></li>
                    <li class="active">Promociones</li>
						<li class="active"> </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
		
		
	   <div class="row">
		<div class="col-md-12">	   
		@foreach($promotions as $promotion)
			<div class="col-md-4 card-promotion">	   
				<div class="media">
						<div class="pull-left">
							<span class="fa-stack fa-2x">																	
								  <div class="avatar-circle text-primary">
									<a href="{{ route('lawyer.contract', $promotion->loginid) }}"><span class="initials">{{ $promotion->name[0] }}</span></a>
								  </div>
							</span> 
						</div>
						<div class="media-body">
							<div>							    
								<a href="{{ route('lawyer.contract', $promotion->loginid) }}"  title="{{ strtoupper($promotion->name) }}">
								  <h5 class="media-heading text-uppercase" >{!! $promotion->shortname !!}</h5>	
								</a>
								<div class="text-muted small">Promocion valida del {{ $promotion->start_date }} al {{ $promotion->end_date }}</div>
								<div>
								  <span class="label label-danger"> @if($promotion->discount == 0) PROMOCION @else {{ $promotion->discount }}% @endif </span>
								</div>							
							</div>
							<div><strong> {{ $promotion->user->lawyer->name." ".$promotion->user->lawyer->lastname }}</strong></div>
							<div>{!! $promotion->shortcontent !!}</div>
						</div>
				</div>
		  </div>	
		@endforeach	
		</div>
		<div class="col-md-12">
			{{ $promotions->render() }}		
		</div>
	   </div>
	</div>
	  
@endsection
