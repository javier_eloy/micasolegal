@extends('template.main')

 
@section('content')

<!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $promotion->name }}</h1>
				<h4><small><strong>Periodo:</strong> {{ $promotion->start_date}} a {{ $promotion->end_date}}</small></h4>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-12">
                <p>{{ $promotion->content }}</p>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
	  
	  
@endsection


@section('js')
	<script src="{{ asset('js/custom.js') }}"></script>
	<script src="{{ asset('js/signup.js') }}"></script>
@endsection