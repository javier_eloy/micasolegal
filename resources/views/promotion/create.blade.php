@extends('template.main')

 
@section('content')

<!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Nueva Promocion</h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                
			{!! Form::open(['route' => 'promotion.store', 'method' => 'POST']) !!}
				
				<div class="form-group">
					{!! Form::label('name', 'Descripcion') !!}
					{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required', 'maxlength' => '100']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('discount', 'Descuento') !!}	
					<div class="row">
					 <div class="col-md-12 form-inline text-nowrap">
						<span>{!! Form::input('number', 'discount', null, ['class' => 'form-control', 'min' => '0', 'max' => '99', 'title' => 'Coloque 0 para no mostrar un porcentaje']) !!}</span>
						<span>%</span>
					 </div>
					</div>
				</div>
					
				
				
				<div class="row">
				  <div class="col-lg-12">
					{!! Form::label('start_date', 'Periodo') !!}
				  </div>
				  <div class="col-lg-12 form-inline">
				  
					<div class="input-daterange" id="datepicker">
					{!! Form::text('start_date', null, ['class' => 'form-control form-date', 'required']) !!}
					<span > a </span>
					{!! Form::text('end_date', null, ['class' => 'form-control form-date', 'required']) !!}
					</div>
					
				  </div>
			   </div>
			   
				<div class="form-group">
					{!! Form::label('content', 'Contenido') !!}
					{!! Form::textarea('content', null, ['class' => 'form-control textarea-content', 'maxlength' => '5000', 'required']) !!}
				</div>

			
				<div class="form-group">
					{!! Form::submit('Crear', ['class' => 'btn btn-primary']) !!}
				</div>
				
			{!! Form::close() !!}

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

@endsection

@section('js')
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css') }}" rel="stylesheet">

<script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script> 
<script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>

<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>

<script>
  $('.input-daterange').datepicker({
		format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "es"
    });
</script>
    

@endsection