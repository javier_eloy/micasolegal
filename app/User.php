<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract 
{

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 't000_login';
	protected $primaryKey = 'loginid';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'email', 'password', 'roleid', 'image_path', 'is_lawyer','countryid','cityid'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	
	public function lawyer()
	{
	  return $this->hasOne('App\Lawyer','loginid');	
	}
	
	public function article()
	{
	  return $this->hasMany('App\Article','loginid');		
	}

	public function scopeLawyers($query)
	{

	  return $query->where('is_lawyer', 1);
	}
	
	public function scopeUsers($query)
	{

	  return $query->where('is_lawyer', 0);
	}
	
	public function country()
	{
	  return $this->belongsTo('App\Country','countryid');
	}
	
	public function getTranslateRoleAttribute()
	{
	
        switch($this->level)
		{
	     case LEVEL_USER: return "Usuario";
		 case LEVEL_EDITOR: return "Editor";
		 case LEVEL_ADMIN: return "Administrador";			
		 default : return "(Desconocido)";
		}
	}
	
}
