<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodePromotion extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 't102_code_promotions';
	protected $primaryKey = 'cpromotionid';
	
	protected $fillable =['name','cost','allow_article','default','n_promo'];
	
	
	
	
}
