<?php

namespace App\Factories;

use App\User;
use App\Repositories\ActivationRepository;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;

class ActivationFactory
{
    protected $activationRepo;
    protected $mailer;
    protected $resendAfter = 24;

    public function __construct(ActivationRepository $activationRepo, Mailer $mailer)
    {
        $this->activationRepo = $activationRepo;
        $this->mailer = $mailer;
    }

    public function sendActivationMail($user)
    {
        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user);

		// Create message to send
        $link = route('user.activate', $token);
    /*    $message = sprintf('Activar cuenta utilizando el siguiente enlace: %s', $link, $link);

        $this->mailer->raw($message, function (Message $m) use ($user) {
            $m->to($user->email)->subject('Correo de Activación');
        });*/
		
		 $link = route('user.activate', $token);

		 $this->mailer->send('emails.activation', array(
									'email' => $user->email,
									'username' => $user->username,
									'link' => $link
									), function($message) use ($user)
									{										
										$message->to($user->email, 'Codigo de Activación')->subject('Activación de cuenta en tepublico.es');
									});
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;
    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }
}