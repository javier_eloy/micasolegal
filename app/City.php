<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
	protected $table = 't002_city';
	protected $primaryKey = 'cityid';
	
	protected $fillable = ['name', 'countryid'];
	
	public function Country()
	{
	  return $this->belongsto('App\Country','countryid');	
	}
	
	public function Lawyer()
	{
	 return $this->hasMany('App\Lawyer','cityid');	
	}
	
}
