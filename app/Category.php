<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
	protected $table = 't004_category';
	protected $primaryKey = 'categoryid';
	
	protected $fillable = ['name'];
	
	public function article()	
	{
	   return $this->hasMany('App\Article','categoryid');
	}
	
	public function comment()
	{
	   return $this->hasManyThrough('App\Comment','App\Article','categoryid','articleid');
	}
	
	/* public function article()
	{
	   return $this->belongsTo('App\Article','categoryid');
	}*/
	
	/*public function category()
	{
		return $this->hasMany('App\Category','categoryid');
	}*/
}
