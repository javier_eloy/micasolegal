<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
	protected $table = 't011_section';
	protected $primaryKey = 'sectionid';
	
	protected $fillable = ['name'];
	
	public function article()	
	{
	   return $this->hasMany('App\Article','sectionid');
	}
}
