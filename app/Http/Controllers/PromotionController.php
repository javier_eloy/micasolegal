<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;


use App\Promotion;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
	   $promotions = Promotion::whereDate('end_date','>=',date('Y-m-d'))->orderBy('created_at','DESC')->paginate(12);
        
		foreach($promotions As $promotion)
		{
			 $promotion->user;
			 $promotion->user->lawyer;
			 
		}
		
		return view('promotion.index')
				->with('promotions',$promotions);
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		
        //		
		return view('promotion.create');
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$promotion = new Promotion($request->only(['name','content','discount']));

		$promotion->start_date=Carbon::createFromFormat('d/m/Y', $request->input('start_date'));
		$promotion->end_date=Carbon::createFromFormat('d/m/Y', $request->input('end_date'));
		$promotion->loginid = \Auth::user()->loginid;
		$promotion->save();
		
		return redirect()->route('user.mypromotion');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$promotion=Promotion::find($id);
		
		return view('promotion.show')
				->with('promotion',$promotion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$promotion= Promotion::find($id);
		

		return view('promotion.edit')
				 ->with('promotion',$promotion);
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$promotion = Promotion::find($id);

		$promotion->name=$request->input('name');
		$promotion->content=$request->input('content');
		$promotion->start_date=Carbon::createFromFormat('d/m/Y', $request->input('start_date'));
		$promotion->end_date=Carbon::createFromFormat('d/m/Y', $request->input('end_date'));		
		$promotion->save();
		
		return redirect()->route('user.mypromotion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
