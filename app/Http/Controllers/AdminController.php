<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Http\Requests\CountryRequest;
use App\Http\Requests\ProfesionRequest;
use App\Http\Requests\CityRequest;


use App\User;
use App\Country;
use App\Profesion;
use App\City;
use App\CodePromotion;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		
		return view('administration.index');	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	/**
	* Config profiles
	*
	*/
	public function config()
	{

		return view('administration.config');
	}
	
	/**
	* Show user profiles
	*
	*/
	public function profiles()
	{
		
	 $lawyers = User::Lawyers()->get();
	 $users=User::Users()->get();
		
	  return view('administration.profiles')
			->with('lawyers',$lawyers)
			->with('users',$users);	
	}
	
	/**
	* Show user promotion
	*
	*/
	public function promotion()
	{
		
	  $promotions = CodePromotion::get();
	  
	  return view('administration.promotion')
			->with('promotions', $promotions);	
	}
	
	/**
	* Show user profile on admin
	*
	*/
	public function profile_edit($id)
	{
		
		$profile = User::find($id);
		
		 return view ('administration.profile_edit')
				->with('profile',$profile)
				->with('levels',Config::get('constants.levels'));
		
	}
	
	/*
	*  Update on database
	*
	*/
	public function profile_update(Request $request, $id)
	{
		
		$profile = User::find($id);
		
		$profile->level=$request->level;
		$profile->email=$request->email;
		$profile->save();
		
		Flash::success('El usuario '. $profile->name .' ha sido actualizado con exito')->important();
		return redirect()->route('admin.profiles');
		
	}
	
	
	/*
	* Show Country list
	*
	*/
	public function country()
	{
		$country = Country::paginate(15);
		
		return view('administration.country')
					->with("countrys",$country);
	}

	public function country_insert(CountryRequest $request)
	{
		$country = new Country;
		$country->name = $request->name;
		$country->initial = $request->initial;
		$country->save();
		
		return redirect()->route('admin.country');
	}
		
	public function country_destroy(Request $request)
	{
		$checked = $request->only('checked')['checked'];
		$all_delete=true;
		
		if($checked){
			foreach($checked as $check)
			{
			  $country = Country::find($check);			 
			  if(count($country->lawyer) == 0){
				  $country->delete();
			  } else {
				  $all_delete=false;
			  }
			}
		}
		if(!$all_delete) Flash::warning('Existen paises que no lograron eliminarse debido a que existe relación con otras tablas')->important();
		
		return redirect()->route('admin.country');
	}
	
	/*****
	*
	* Show profesion
	*
	****/

	public function profession()
	{
		$profesions = Profesion::paginate(15);
		
		return view('administration.profession')
					->with("professions",$profesions);
	}

	public function profession_insert(ProfesionRequest $request)
	{
		$profesions = new Profesion;
		$profesions->name = $request->name;
		$profesions->save();
		
		return redirect()->route('admin.profession');
	}
		
	public function profession_destroy(Request $request)
	{
		$checked = $request->only('checked')['checked'];
		$all_delete=true;
		
		if($checked){
			foreach($checked as $check)
			{
			  $profesion = Profesion::find($check);	
		  
			  if(count($profesion->lawyer) == 0){
				  $profesion->delete();
			  } else {
				  $all_delete=false;
			  }
			}
		}
		if(!$all_delete) Flash::warning('Existen profesiones que no lograron eliminarse debido a que existe relación con otras tablas')->important();
		
		return redirect()->route('admin.profession');
	}
	
	/*****
	*
	* Show cityes
	*
	****/

	public function city()
	{
		$countries = Country::orderby('name','ASC')->lists('name', 'countryid');
		$cities = City::orderby('countryid')->paginate(15);
		foreach($cities as $city)
		{
		   $city->country;
		}
		
		return view('administration.city')
					->with("cities",$cities)
					->with("countries",$countries);
	}

	public function city_insert(CityRequest $request)
	{
		$city = new City;
		$city->countryid = $request->countryid;
		$city->name = $request->name;
		$city->save();
		
		return redirect()->route('admin.city')->withInput();
	}
		
	public function city_destroy(Request $request)
	{
		$checked = $request->only('checked')['checked'];
		$all_delete=true;
		
		if($checked){
			foreach($checked as $check)
			{
			  $city = City::find($check);	
			  if(count($city->lawyer) == 0){
				  $city->delete();
			  } else {
				  $all_delete=false;
			  }
			}
		}
		if(!$all_delete) Flash::warning('Existen ciudades que no lograron eliminarse debido a que existe relación con otras tablas')->important();
		
		return redirect()->route('admin.city')->withInput();
	}
	
	

}
