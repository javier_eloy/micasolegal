<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Article;
use App\Section;
use App\Category;
use App\Tag;
use App\Option;
use App\Comment;


class BlogController extends Controller
{
	

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
		
		$option = Option::where('type','=',ARTICLE_BLOG)->first();

		$blogs = Article::Blog(true)->orderBy('created_at','DESC')->paginate(6);
		$blogs->each(function($blog) use ($option) {
			if ($option->has_category == 1) $blog->category;
			if ($option->has_section == 1) $blog->section;
			$blog->login;
		});
		
	
		$sections = ($option->has_section == 1) ? Section::orderBy('name','ASC')->get() : array();
		$categories = ($option->has_category == 1) ? Category::orderby('name','ASC')->lists('name','categoryid') : array();
		$tags = ($option->has_tags == 1) ? Tag::orderby('name','ASC')->lists('name','tagid') : array();
		
		return view('blog.index')
			->with('blogs',$blogs)
			->with('sections',$sections)
			->with('categories',$categories)
			->with('tags',$tags);
		
    }
	
	public function section(Request $request, $id)
    {
			
		$option = Option::where('type','=',ARTICLE_BLOG)->first();

		$blogs = Article::Blog(true)->where('sectionid','=',$id)->orderBy('created_at','DESC')->paginate(6);
		$blogs->each(function($blog) use ($option) {
			if ($option->has_category == 1) $blog->category;
			if ($option->has_section == 1) $blog->section;
			$blog->login;
		});
		
	
		$sections = ($option->has_section == 1) ? Section::orderBy('name','ASC')->get() : array();
		$categories = ($option->has_category == 1) ? Category::orderby('name','ASC')->lists('name','categoryid') : array();
		$tags = ($option->has_tags == 1) ? Tag::orderby('name','ASC')->lists('name','tagid') : array();
		
		return view('blog.index')
			->with('blogs',$blogs)
			->with('sections',$sections)
			->with('categories',$categories)
			->with('tags',$tags);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		
		if(\Auth::user()->is_lawyer == 1)
		{
		
		$option = Option::where('type','=',ARTICLE_BLOG)->first();

		$sections = ($option->has_section == 1) ? Section::orderBy('name','ASC')->lists('name','sectionid') : array();
		$categories = ($option->has_category == 1) ? Category::orderby('name','ASC')->lists('name','categoryid') : array();
		$tags = ($option->has_tags == 1) ? Tag::orderby('name','ASC')->lists('name','tagid') : array();
		
			return view('blog.create')
					->with('sections',$sections)
					->with('categories',$categories)
					->with('tags',$tags)
					->with('option',$option);
		} else {
			Flash::warning('Debe ingresar como Abogado para crear articulos')->important();
			return  redirect()->back();
		}
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$option = Option::where('type','=',ARTICLE_BLOG)->first();
		
		DB::beginTransaction();
		try{
			$name = "";
			if($request->file('image'))
			{
				$file = $request->file('image');
				$name = 'micasolegal_blog_' . time() . '.'. $file->getClientOriginalExtension();
				$path = public_path(). '/images/user/';
				$file->move($path, $name);
			}
			
			$request_field = ['title','content'];
			if($option->has_section) array_push($request_field,'sectionid');
			if($option->has_category) array_push($request_field, 'categoryid');
		
			$blog = new Article($request->only($request_field));			
			
			switch($option->type_mod) 
			{
				case OPT_MOD_ADMIN: $blog->type=ARTICLE_BLOG_MODERATE; break;
				default: $blog->type=ARTICLE_BLOG; break;
			}
			$blog->image_path = $name;
			$blog->loginid = \Auth::user()->loginid;
			$blog->save();		
			
			if($option->has_tags) $blog->tags()->sync($request->tags);
			
			switch($option->type_mod) {
				case OPT_MOD_NONE: Flash::success('Se ha creado el articulo '. $blog->title . ' con exito !')->important(); break;
				case OPT_MOD_ADMIN: Flash::info('El articulo '. $blog->title . ' está siendo revisado y se mostrará en cuanto sea aprobado')->important(); break;
			}
			
		} catch(ValidationException $e) {
			DB::rollback(); 
			return redirect()->route('blog.create')
					->withErrors($e->getErrors())
					->withInput();
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		DB::commit();
		
		return redirect()->route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$blog = Article::find($id);		
		$blog->login;
		$blog->comment=$blog->comment->where('type',COMMENT);
		
		$section= Section::all();
		
		return view('blog.show')
				->with('blog',$blog)
				->with('section', $section);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * Add comment to blog
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment(Request $request, $id)
    {
        //
		$option = Option::where('type','=',ARTICLE_BLOG)->first();
		
		$comment = new Comment();
		$comment->text = $request->text;
		$comment->articleid = $id;
		$comment->loginid = \Auth::user()->loginid;
		
		switch($option->type_cmod)
		{
			case OPT_CMOD_ADMIN: 
						$comment->type = COMMENT_MODERATE; 
						Flash::warning('Su comentario está siendo revisado, tan pronto sea aprobado será publicado')->important();
						break;
			case OPT_CMOD_ADMIN_OWNER: 
						$comment->type = COMMENT_MODERATE_USER; 
						Flash::warning('Su comentario está siendo revisado, tan pronto sea aprobado será publicado')->important();
						break;
			default: $comment->type=COMMENT; 					 
					 break;
		}
		
		$comment->save();
		
		return redirect()->route('blog.show',$id);

    }
}
