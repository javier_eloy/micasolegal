<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Lawyer;
use App\User;
use App\Hire;
use App\Country;
use App\Profesion;
use App\Promotion;
use App\Code;

class LawyerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$lawyers = Lawyer::orderBy('name', 'ASC')->paginate(20);	
		/*$lawyers->each(function($lawyers){
			$lawyers->profesion;
			$lawyers->login;
		});	*/	
		
		return view('lawyer.index')
				->with('lawyers', $lawyers);
    }

    /**
     * Show the form for creating a new contract.
     *
     * @return \Illuminate\Http\Response
     */
    public function contract(Request $request)
    {
        //
		$lawyer_id = $request->lawyer_id;
		
		$lawyer = User::find($lawyer_id);
		$lawyer->lawyer;
		$lawyer->lawyer->profesion;
		
		$promotions = Promotion::where('active','=',1)->where('loginid','=',$lawyer->loginid)->simplePaginate(15);
		$lawyer_client = $lawyer->lawyer->code_where('client')->lists('value')->toArray();
		$lawyer_language = $lawyer->lawyer->code_where('language')->lists('value')->toArray();
		$lawyer_offer_service = $lawyer->lawyer->code_where('offer_service')->lists('value')->toArray();
		$lawyer_fare = $lawyer->lawyer->code_where('fare')->lists('value')->toArray();
		$lawyer_payment_mode = $lawyer->lawyer->code_where('payment_mode')->lists('value')->toArray();
		$lawyer_payment_type = $lawyer->lawyer->code_where('payment_type')->lists('value')->toArray();
		$lawyer_special = $lawyer->lawyer->code_where('special')->lists('value')->toArray();

		return view('lawyer.contract')
				->with('lawyer',$lawyer)
				->with('promotions',$promotions)
				->with('lawyer_client',$lawyer_client)
				->with('lawyer_language',$lawyer_language)
				->with('lawyer_offer_service',$lawyer_offer_service)
				->with('lawyer_fare',$lawyer_fare)
				->with('lawyer_payment_mode',$lawyer_payment_mode)
				->with('lawyer_payment_type',$lawyer_payment_type)
				->with('lawyer_special',$lawyer_special);
    }

    /**
     * Hire a lawyer
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function hire(Request $request)
    {
    
		//
		$lawyer_id = $request->lawyer_id;
		$user_id = \Auth::user()->loginid;
		
		if( $lawyer_id == $user_id) {
			Flash::info('No se puede contratar a si mismo')->important();
			return back();
		}
		
		$hire = Hire::where([
					['user_id','=',$user_id],
					['lawyer_id','=',$lawyer_id],
					['active','=',1]
				])->first();
				
		if($hire == null)
		{
			// Save hire
			$hire = new Hire();
			$hire->lawyer_id = $lawyer_id;
			$hire->user_id = $user_id;
			$hire->message = $request->message;
			$hire->save();
		
			return redirect()->route('lawyer.showhire', ['lawyer_id' => $lawyer_id]);
			
		} else {
			Flash::info("Ya tiene un servicio contratado con el abogado")->important();
			return back();		
		}
		
    }

    /**
     * Display the specified hire.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showhire($lawyer_id)
    {
		$user_id = \Auth::user()->loginid;
		$hire = Hire::where([
					['user_id','=',$user_id],
					['lawyer_id','=',$lawyer_id],
					['active','=',1]
				])->first();
		
		$hire->lawyers;
		$hire->lawyers->login;

		return view('lawyer.hire')
				->with('hire', $hire);
				
    }
	
	 /**
     * Rate service hired
     *
     * @return \Illuminate\Http\Response
     */
    public function ratehire(Request $request)
    {
	  $user_id = \Auth::user()->loginid;
	  $lawyer_id = $request->lawyer_id;
	  $rate = $request->rate;
	  $message = $request->message;
	  
	  $hire = Hire::where([
					['user_id','=',$user_id],
					['lawyer_id','=',$lawyer_id],
					['active','=',1]
				])->first();
				

		DB::beginTransaction();
		
		try{		
		
			  $hire->message = $message;
			  $hire->rate = $rate;
			  $hire->active = 0;	
			  $hire->save(); 
			  
			  $lawyer = Lawyer::where('lawyerid',$lawyer_id)->first();
			  $lawyer->rate=$lawyer->RateLawyer;
			  $lawyer->save();
			  
		} catch(ValidationException $e) {
			DB::rollback(); 
			return redirect()->route('lawyer.showhire')
					->withErrors($e->getErrors())
					->withInput();
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		DB::commit();
	   
	  return redirect()->route('user.myhire');
    }
	

    /**
     * Find a lawyer
     *
     * @return \Illuminate\Http\Response
     */
	public function find($specialid = 0)
	{
		$countryid = 0;
		
		$ip=$this->getRealIpAddr();	
		$details = ( filter_var($ip, FILTER_VALIDATE_IP) ) ? json_decode(file_get_contents("http://ipinfo.io/{$ip}/geo")) : json_decode(file_get_contents("http://ipinfo.io"));

		if(isset($details->country)) {
			$country_code=$details->country; 
			$current_country = Country::where('initial',$country_code)->first();
			if($current_country)
				$countryid=$current_country->countryid;
		}
		
		
		$country = Country::orderBy('name', 'ASC')->lists('name', 'countryid');		
		$profesion = Profesion::orderBy('name', 'ASC')->lists('name', 'profesionid');		
		$specialities = Code::Specialities()->lists('value', 'codeid');		
		
		$profesion[0] = "(Todos)";
		$specialities[0] = "(Todos)";
		
	    return view('lawyer.find')
				->with('country',$country)
				->with('profesion',$profesion)
				->with('countryid',$countryid)
				->with('specialities',$specialities)
				->with('specialid',$specialid);
	
	}
	
    /**
     * Perform a search
     *
     * @return \Illuminate\Http\Response
     */
	public function search(Request $request)
	{
		$where_lawyer = array();
		if($request->profesionid && $request->profesionid != 0) $where_lawyer['profesionid'] = $request->profesionid;
		

	/*	
		if($request->cityid)  $where_user['cityid'] = $request->cityid;
		if($request->countryid) $where_user['countryid'] = $request->countryid;
		if($request->specialid && $request->specialid != 0) $where_special['codeid'] = $request->specialid;	
	    $lawyers = Lawyer::where($where_lawyer)
					->wherehas('login', function($q) use ($where_user){
						$q->where($where_user);
					})
					->wherehas('code', function($q) use ($where_special){
						$q->where($where_special);
					})
					->orderBy('name')->paginate(20);*/
					
		$lawyers =  Lawyer::where($where_lawyer)
						->join('t000_login',function ($join) use($request) {
							  $join->on('t000_login.loginid', '=', 't001_lawyer.loginid');
							  if($request->cityid)
								$join->on('t000_login.cityid','=',DB::raw($request->cityid));
							  if($request->countryid)
								$join->on('t000_login.countryid','=',DB::raw($request->countryid));
						})
						->join('t101_code_lawyers', function($join) use($request) {
							  $join->on('t101_code_lawyers.lawyerid', '=', 't001_lawyer.lawyerid');
							  if($request->specialid && $request->specialid != 0) {
								   $join->on('t101_code_lawyers.type','=' , DB::raw('"special"'));							  
								   $join->on('t101_code_lawyers.codeid','=' , DB::raw($request->specialid));							  
							  }
						})
						->select('t001_lawyer.*')
						->distinct()
						->orderBy('name')->paginate(20);

		if($lawyers->count()) {
			$returnHTML = view('lawyer.search')->with('lawyers',$lawyers)->render();
			return response()->json(['success' => true , 'html' => $returnHTML ]);
		} else {
			
			return response()->json(['success' => false]);
		}
	
	}

	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	private function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
   }
}
