<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Laracasts\Flash\Flash;

/*use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;*/

use Validator;
use App\Factories\ActivationFactory;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;



class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
    }*/
	protected $activationFactory;

	public function __construct(ActivationFactory $activationFactory)
	{
		$this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
		$this->activationFactory = $activationFactory;
	}

	public function activateUser($token)
	{
		if ($user = $this->activationFactory->activateUser($token)) {
			auth()->login($user);
			Flash::success("Su cuenta ha sido activada" )->important();
			return redirect($this->redirectPath());
		}
		abort(404);
	}

	public function authenticated(Request $request, $user)
	{
		if (!$user->activated) {
			$this->activationFactory->sendActivationMail($user);
			auth()->logout();
			Flash::info("Necesita confirmar su cuenta. Se ha enviado el codigo de activación. revise su correo" )->important();
			return back();
		}
		return redirect()->intended($this->redirectPath());
	}
	
	protected $redirectTo = '/';
	protected $loginPath = '/auth/login';
	protected $username = 'username';


	

}
