<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;


use App\Factories\ActivationFactory;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Country;
use App\City;
use App\User;
use App\Lawyer;
use App\Profesion;
use App\Article;
use App\Comment;
use App\Promotion;
use App\Hire;
use App\Code;
use App\CodePromotion;

class UserController extends Controller
{
	
	
	protected $activationFactory;

	public function __construct(ActivationFactory $activationFactory)
	{		
		$this->activationFactory = $activationFactory;
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
    }
	/**
	*
	* Display a listing of my moderation
	*
	*/
	public function mymoderate()
	{
		$id = \Auth::user()->loginid;
		$level = \Auth::user()->level;
		$admin_amoderates=array();
		$admin_cmoderates=array();
		
		// Comment to aprove by user
		$cmoderates = Comment::with('article')->whereHas('article', function ($query) use ($id) {
							$query->where('loginid','=', $id);			
					 })->where('type','=',COMMENT_MODERATE_USER)->get();

		
		if($level == LEVEL_ADMIN)
		{
			// Articles to moderate by admin
			$admin_amoderates = Article::wherein('type',[ARTICLE_BLOG_MODERATE,ARTICLE_QUESTION_MODERATE,ARTICLE_FORUM_MODERATE])->get(); 
			
			// Comment to aprove by admin
			$admin_cmoderates = Comment::with('article')->where('type','=',COMMENT_MODERATE)->get();
		}
		
		return view('user.mymoderate')
					->with('cmoderates',$cmoderates)
					->with('admin_amoderates',$admin_amoderates)
					->with('admin_cmoderates',$admin_cmoderates);
		
	}
	
	/**
	*
	* Display a listing of my promotion
	*
	*/
	public function mypromotion()
	{
		$id = \Auth::user()->loginid;
		$promotions = Promotion::where('active','=',1)->where('loginid','=',$id)->simplePaginate(15);
		
		return view('user.mypromotion')
				->with('promotions',$promotions);
	}
	
	/**
	*
	* Display a listing of my questions
	*
	*/
	public function mypost()
	{
		$id = \Auth::user()->loginid;
		
		$questions = Article::Question()->where('loginid','=',$id)->simplePaginate(15);
		
		return view('user.mypost')
				->with('questions',$questions);
	}
	
	/**
	*
	* Display a listing of my questions
	*
	*/
	public function myanswer()	
	{
		$id = \Auth::user()->loginid;
		
		$articles =  Article::whereHas('comment', function ($query) use ($id) {
							$query->where('loginid','=', $id);			
					})->paginate(15);
					
		$articles->each(function($article) {
			$article->comment;
		});
		

		//$comments = Comment::where('loginid','=',$id)->simplePaginate(15);
				
		return view('user.myanswer')
				->with('articles',$articles);
	}
	
	/**
	*
	* Display a listing of my hires
	*
	*/
	public function myhire()	
	{
		$user_id = \Auth::user()->loginid;
		
		$hires =  Hire::where([
					['user_id','=',$user_id],
					['active','=',1]
				])->paginate(15);
					
		$hires->each(function($hire) {
			$hire->lawyers;
		});

			
		return view('user.myhire')
				->with('hires',$hires);
	}
	
	/**
	*
	* Display a listing of who hire me
	*
	*/
	public function whohire()	
	{
		$user_id = \Auth::user()->loginid;
		$hires = null;

        $user = User::find($user_id);
		if($user->is_lawyer == 1) {		
			
			$user->lawyer;
			$hires =  Hire::where([
					['lawyer_id','=',$user_id],
					['active','=',1]
				])->paginate(15);
			$hires->each(function($hire) {
				$hire->user;
			});
					
		}
			
		return view('user.whohire')
			->with('hires',$hires);
	}

	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$country = Country::orderBy('name', 'ASC')->lists('name', 'countryid');		
		$profesion = Profesion::orderBy('name', 'ASC')->lists('name', 'profesionid');
		$client = Code::Client()->get();
		$language = Code::Language()->get();
		$service =  Code::OfferService()->get();
		$fare =  Code::Fare()->get();
		$payment_mode =  Code::PaymentMode()->get();
		$payment_type=  Code::PaymentType()->get();
		$specialities=  Code::Specialities()->get();
		

		return view('user.create')
			   ->with('country',$country)
			   ->with('profesion',$profesion)
			   ->with('client',$client)
			   ->with('language',$language)
			   ->with('service',$service)
			   ->with('fare',$fare)
			   ->with('payment_mode',$payment_mode)
			   ->with('payment_type',$payment_type)
			   ->with('specialities',$specialities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //
		
		//MANIPULACION DE IMAGENES
		$name="";
	
		DB::beginTransaction();
		try{
			
			if($request->file('image'))
			{
				$file = $request->file('image');
				$name = 'micasolegal_' . time() . '.'. $file->getClientOriginalExtension();
				$path = public_path(). '/images/user/';
				$file->move($path, $name);
			}
			
			
			$user = new User($request->only(['username','password','email','image_path','countryid','cityid']));
			$user->is_lawyer = $request->has('is_lawyer') ;
			$user->password = bcrypt($request->password);
			$user->image_path = $name;
			$user->save();
						
			if($request->input('is_lawyer',false) == true )
			{ 
				$lawyer = new Lawyer($request->only(['name','lastname','phone', 'postalcode','address','resume','profesionid', 'experience','partnership','service','schedule']));
				$lawyer->login()->associate($user);
				$lawyer->save();
				
				
				$sync_client = null;
				
				if($request->client)
				foreach($request->client As $value){
				  $sync_client[$value] = array('type' => 'client');
				}			  
				
				if($request->language)
				foreach($request->language As $value){
				  $sync_client[$value] = array('type' => 'language');
				}	
				
				if($request->offer_service)
				foreach($request->offer_service As $value){
				  $sync_client[$value] = array('type' => 'offer_service');
				}
				
				if($request->fare)
				foreach($request->fare As $value){
				  $sync_client[$value] = array('type' => 'fare');
				}
				
				if($request->payment_mode)
				foreach($request->payment_mode As $value){
				  $sync_client[$value] = array('type' => 'payment_mode');
				}
				
				if($request->payment_type)
				foreach($request->payment_type As $value){
				  $sync_client[$value] = array('type' => 'payment_type');
				}
				
				if($request->special)
				foreach($request->special As $value){
				  $sync_client[$value] = array('type' => 'special');
				}
				// Save code table
				if($sync_client) {
					$lawyer->code()->sync($sync_client);
				}

			}
			
			// Send Email
			$this->activationFactory->sendActivationMail($user);
		
		} catch(ValidationException $e) {
			DB::rollback(); 
			return redirect()->route('user.create')
					->withErrors($e->getErrors())
					->withInput();
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		DB::commit();
		
		Flash::warning("Se ha enviado el codigo de activación a su correo.Por favor revise." )->important();
		return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		
		$CountArticles = Article::get()->count();
		$CountComment = Comment::get()->count();

		
		$CountMyArticles = Article::where('loginid','=',$id)->count();
		$CountMyComment = Comment::where('loginid','=',$id)->count();
		
		return view('user.show')
			->with('CountArticles',$CountArticles)
			->with('CountComment',$CountComment)
			->with('CountMyArticles',$CountMyArticles)
			->with('CountMyComment',$CountMyComment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$user = User::find($id);		
		$country = Country::orderBy('name', 'ASC')->lists('name', 'countryid');		
		$city = City::where('countryid','=', $user->countryid)->lists('name','cityid');
		
		
		
		if(\Auth::user()->is_lawyer == 1)
		{
			$user->lawyer;
			
			$load_code = $user->lawyer->code()->lists('value','t101_code_lawyers.codeid')->toArray();
						
			$profesion = Profesion::orderBy('name', 'ASC')->lists('name', 'profesionid');		
			
			$client = Code::Client()->get();			
			$language = Code::Language()->get();
			$service =  Code::OfferService()->get();			
			$fare = Code::Fare()->get();			
			$payment_mode =  Code::PaymentMode()->get();			
			$payment_type=  Code::PaymentType()->get();
			$specialities=  Code::Specialities()->get();
			$code_promotion = CodePromotion::get();
		
			return view('user.edit_lawyer')
					->with('country',$country)
					->with('city',$city)
					->with('profesion',$profesion)
					->with('user',$user)
					->with('client',$client)
					->with('language',$language)
					->with('service',$service)
					->with('fare',$fare)
					->with('payment_mode',$payment_mode)
					->with('payment_type',$payment_type)
					->with('load_code',$load_code)
					->with('code_promotion',$code_promotion)
					->with('specialities',$specialities);
					
		} else
			return view('user.edit_single')
					->with('user',$user)
					->with('country',$country)
					->with('city',$city);
;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        //
		$lawyer_update=false;		
		$oldfile = "";
		
		$user = User::find($id);
		
		DB::beginTransaction();
		try{
			
			if($request->file('image'))
			{
				$file = $request->file('image');
				$name = 'micasolegal_' . time() . '.'. $file->getClientOriginalExtension();
				$path = public_path(). '/images/user/';
				$file->move($path, $name);
				$oldfile = $path.$user->image_path;
				$user->image_path = $name;			
			}
		
			if(!empty($request->password))
				$user->password=bcrypt($request->password);
			
			$user->countryid = $request->countryid;
			$user->cityid = $request->cityid;
			$user->save();
			
			if(!empty($oldfile))
				\File::delete($oldfile);
			
			if(!is_null($request->input('is_lawyer')))
			{
				$lawyerUser=['name' => $request->name, 'lastname' => $request->lastname,						 
							 'phone' => $request->phone, 'profesionid' => $request->profesionid,
							 'postalcode' => $request->postalcode, 'address' => $request->address,
							 'resume' => $request->resume ,'profesionid' => $request->profesionid, 
							 'experience' => $request->experience,'partnership' => $request->partnership,
							 'service' => $request->service,'schedule' => $request->schedule ];
				$user->lawyer->update($lawyerUser);
				
				$sync_client = null;
					
				if($request->client)
				foreach($request->client As $value){
				  $sync_client[$value] = array('type' => 'client');
				}			  
				
				if($request->language)
				foreach($request->language As $value){
				  $sync_client[$value] = array('type' => 'language');
				}	
				
				if($request->offer_service)
				foreach($request->offer_service As $value){
				  $sync_client[$value] = array('type' => 'offer_service');
				}
				
				if($request->fare)
				foreach($request->fare As $value){
				  $sync_client[$value] = array('type' => 'fare');
				}
				
				if($request->payment_mode)
				foreach($request->payment_mode As $value){
				  $sync_client[$value] = array('type' => 'payment_mode');
				}
				
				if($request->payment_type)
				foreach($request->payment_type As $value){
				  $sync_client[$value] = array('type' => 'payment_type');
				}
				
				if($request->special)
				foreach($request->special As $value){
				  $sync_client[$value] = array('type' => 'special');
				}
				// Save code table
				if($sync_client) {
					$user->lawyer->code()->sync($sync_client);
				}
			}		
		} catch(ValidationException $e) {
			DB::rollback(); 
			return redirect()->route('user.edit')
					->withErrors($e->getErrors())
					->withInput();
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		
		DB::commit();
		Flash::success('El usuario '. $user->name .' ha sido editado con exito')->important();
		return redirect()->route('user.edit',$id);
		
		
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 /**
     * Aprove blog/forum/question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function a_aprove($id)
    {
        //
		$article = Article::find($id);
		
		switch($article->type)
		{
			case ARTICLE_QUESTION_MODERATE:
					$article->type = ARTICLE_QUESTION;
					break;
			case ARTICLE_BLOG_MODERATE:
					$article->type = ARTICLE_BLOG;
					break;
			case ARTICLE_FORUM_MODERATE:
					$article->type = ARTICLE_FORUM;
					break;
			
		}
		$article->save();
		
		return redirect()->route('user.mymoderate');
		
    }

		 /**
     * Aprove comment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function c_aprove($id)
    {
        //
		$comment = Comment::find($id);
		
		switch($comment->type)
		{
			case COMMENT_MODERATE_USER:
					$comment->type = COMMENT_MODERATE;
					break;
			case COMMENT_MODERATE:
					$comment->type = COMMENT;
					break;
			
		}
		$comment->save();
		
		return redirect()->route('user.mymoderate');
		
    }
	
    public function c_reject($id)
	{
		$comment = Comment::where('commentid','=',$id);
		
		$comment->delete();
		return redirect()->route('user.mymoderate');
		
	}
	
    public function a_reject($id)
	{
		$article = Article::where('articleid','=',$id);
		$article->delete();
		return redirect()->route('user.mymoderate');		
	}



}
