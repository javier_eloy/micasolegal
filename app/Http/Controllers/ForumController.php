<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Article;
use App\Section;
use App\Category;
use App\Tag;
use App\Option;
use App\Comment;

class ForumController extends Controller
{
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //	
		$forums = Category::with(['article' => function ($query) {$query->where('type','=','F');}])
						 ->with(['comment' => function ($query) {$query->where('t006_article.type','=','F');}])	
						 ->get();

		return view('forum.index')
			->with('forums',$forums);
    }

	public function lists($id)
	{
		$categoryname = Category::find($id)->first()->name;
	    $forums = Article::Forum(true)->where('categoryid','=',$id)->get();
		
		
		return view('forum.lists')
				->with('categoryname',$categoryname)
				->with('forums',$forums);
       	
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$option = Option::where('type','=',ARTICLE_FORUM)->first();

		$sections = ($option->has_section == 1) ? Section::orderBy('name','ASC')->lists('name','sectionid') : array();
		$categories = ($option->has_category == 1) ? Category::orderby('name','ASC')->lists('name','categoryid') : array();
		$tags = ($option->has_tags == 1) ? Tag::orderby('name','ASC')->lists('name','tagid') : array();
		
		return view('forum.create')
				->with('sections',$sections)
				->with('categories',$categories)
				->with('tags',$tags)
				->with('option',$option);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$option = Option::where('type','=',ARTICLE_FORUM)->first();
		$name = "";
		
		DB::beginTransaction();
		try{
			if($request->file('image'))
			{
				$file = $request->file('image');
				$name = 'micasolegal_blog_' . time() . '.'. $file->getClientOriginalExtension();
				$path = public_path(). '/images/user/';
				$file->move($path, $name);
			}
			
			$request_field = ['title','content'];
			if($option->has_section) array_push($request_field,'sectionid');
			if($option->has_category) array_push($request_field, 'categoryid');
		
			$forum = new Article($request->only($request_field));			
			
			switch($option->type_mod) 
			{
				case OPT_MOD_ADMIN: $forum->type=ARTICLE_FORUM_MODERATE; break;
				default: $forum->type=ARTICLE_FORUM; break;
			}
			$forum->image_path = $name;
			$forum->loginid = \Auth::user()->loginid;
			$forum->save();		
			
			if($option->has_tags) $forum->tags()->sync($request->tags);
			
			switch($option->type_mod) {
				case OPT_MOD_NONE: Flash::success('Se ha creado el tema '. $forum->title . ' con exito !')->important(); break;
				case OPT_MOD_ADMIN: Flash::info('El tema '. $forum->title . ' está siendo revisado y se mostrará en cuanto sea aprobado')->important(); break;
			}
			
		} catch(ValidationException $e) {
			DB::rollback(); 
			return redirect()->route('forum.create')
					->withErrors($e->getErrors())
					->withInput();
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		DB::commit();
		
		return redirect()->route('forum.index');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

		$forum = Article::find($id);		
		$forum->login;
		$forum->comment;
		$forum->category;

		return view('forum.show')
			->with('forum',$forum);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	/**
     * Add comment to article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment(Request $request, $id)
    {
        //
		$comment = new Comment();
		$comment->text = $request->text;
		$comment->articleid = $id;
		$comment->loginid = \Auth::user()->loginid;
		$comment->save();
		
		return redirect()->route('forum.show',$id);

    }
}
