<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view('contact.index');
    }

   
    /**
     * Send new mail to contact.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(ContactFormRequest $request)
    {
        
	   \Mail::send('emails.contact', array(
									'name' => $request->get('name'),
									'email' => $request->get('email'),
									'tel' => $request->get('tel'),
									'user_message' => $request->get('message')
									), function($message)
									{										
										$message->to('noresponder.tepublico.es@gmail.com', 'Admin')->subject('Contacto a tepublico.es');
									});

		Flash::success('Gracias por contactarnos!');
		return redirect()->route('contact.index');

    }
}
