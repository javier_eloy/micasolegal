<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Category;
use App\Tag;
use App\Article;
use App\Comment;
use Auth;

class QuestionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
		
		$questions = Article::Question(true)->Title($request->title);
		if($request->category) $questions=$questions->Category($request->category);
		if($request->title) $questions=$questions->Title($request->title);
		$questions=$questions->orderBy('created_at','DESC')->simplePaginate(10);
		
		
		$questions->each(function($question) {
			$question->category;
			$question->login;
		});
		
		$categories = Category::orderBy('name','ASC')->get();

		return view('question.index')
			 ->with('questions', $questions)
			 ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		
		if(\Auth::user()->is_lawyer == 0)
		{
			$categories = Category::orderby('name','ASC')->lists('name','categoryid');
			$tags = Tag::orderby('name','ASC')->lists('name','tagid');
			
			return view('question.create')
					->with('categories',$categories)
					->with('tags',$tags);
		} else {
			Flash::warning('Debe ingresar como usuario ordinario para crear una pregunta')->important();
			return  redirect()->back();
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		DB::beginTransaction();
		try{	
			$question = new Article($request->only(['title','content','categoryid']));
			$question->loginid = \Auth::user()->loginid;
			$question->save();
		    if(!empty($request->tags))			
				$question->tags()->sync($request->tags);
			
			Flash::success('Se ha creado la consulta "'. $question->title . '" con exito !')->important();
			
		} catch(ValidationException $e) {
			Flash::error('Errores de Validacion: '. $e->getErrors());
			DB::rollback(); 
			
		} catch(\Exception $e){
			DB::rollback();
			throw $e;
		}
		DB::commit();
		
		return redirect()->route('question.index');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$question = Article::find($id);		
		$question->login;
		$question->comment;
		$question->category;
		
		$categories = Category::all();
		
		return view('question.show')
				->with('question',$question)
				->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * Add comment to article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment(Request $request, $id)
    {
        //
		$comment = new Comment();
		$comment->text = $request->text;
		$comment->articleid = $id;
		$comment->loginid = \Auth::user()->loginid;
		$comment->save();
		
		return redirect()->route('question.show',$id);

    }
	
	
}
