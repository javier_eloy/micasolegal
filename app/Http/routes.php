<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
	'as' 	=>	'index', 
	'uses'	=>	'MainController@index'
	]);
	
Route::get('about', [
		'uses'	=>	'MainController@about',
		'as'	=>	'about'
	]);
	
Route::get('contact', [
		'uses'	=>	'ContactController@index',
		'as'	=>	'contact.index'
	]);
	
Route::post('contact/send',[
		'uses' => 'ContactController@send',
		'as'   => 'contact.send'
	]);


	
Route::get('faq', [
		'uses'	=>	'FaqController@index',
		'as'	=>	'faq.index'
	]);
	
Route::get('lawyer', [
		'uses'	=>	'LawyerController@index',
		'as'	=>	'lawyer.index'
	]);
	
Route::get('lawyer/{lawyer_id}/contract', [
		'uses'	=>	'LawyerController@contract',
		'as'	=>	'lawyer.contract',
		'middleware' => 'auth'
	]);

Route::post('lawyer/{lawyer_id}/hire', [
		'uses'	=>	'LawyerController@hire',
		'as'	=>	'lawyer.hire',
		'middleware' => 'auth'
	]);

Route::post('lawyer/rate', [
		'uses'	=>	'LawyerController@ratehire',
		'as'	=>	'lawyer.rate',
		'middleware' => 'auth'
	]);
	
Route::get('lawyer/{lawyer_id}/showhire', [
		'uses'	=>	'LawyerController@showhire',
		'as'	=>	'lawyer.showhire',
		'middleware' => 'auth'
	]);

Route::get('lawyer/find/{specialid?}', [
		'uses'	=>	'LawyerController@find',
		'as'	=>	'lawyer.find'
	]);
	
// Question 
Route::resource('question', 'QuestionController');

Route::post('question/{id}/comment',[
		'uses' => 'QuestionController@comment',
		'as'   => 'question.store.comment'
	]);

// Blog
Route::resource('blog', 'BlogController');

Route::post('blog/{id}/comment',[
		'uses' => 'BlogController@comment',
		'as'   => 'blog.store.comment'
	]);
	
Route::get('blog/{id}/section',[
		'uses' => 'BlogController@section',
		'as'   => 'blog.section'
	]);
	
Route::post('blog/{id}/comment',[
		'uses' => 'BlogController@comment',
		'as'   => 'blog.store.comment'
	]);	
	
// Foro
Route::resource('forum', 'ForumController');

Route::post('forum/{id}/comment',[
		'uses' => 'ForumController@comment',
		'as'   => 'forum.store.comment'
	]);	
	
Route::get('forum/{id}/lists',[
		'uses' => 'ForumController@lists',
		'as'   => 'forum.lists'
	]);

	
// General
	

// Promocion
Route::resource('promotion', 'PromotionController');


// Users
Route::group(['middleware' => 'auth'], function() {

	Route::get('user/whohire',[
			'uses' => 'UserController@whohire',
			'as'   => 'user.whohire'
		]);
		
	Route::get('user/myhire',[
			'uses' => 'UserController@myhire',
			'as'   => 'user.myhire'
		]);
		
	Route::get('user/mypost',[
			'uses' => 'UserController@mypost',
			'as'   => 'user.mypost'
		]);
		
	Route::get('user/myanswer',[
			'uses' => 'UserController@myanswer',
			'as'   => 'user.myanswer'
		]);	

	Route::get('user/mypromotion',[
			'uses' => 'UserController@mypromotion',
			'as'   => 'user.mypromotion'
	]);	

	Route::get('user/mymoderate',[
			'uses' => 'UserController@mymoderate',
			'as'   => 'user.mymoderate'
	]);	

	Route::get('user/aprove/{id}/comment',[
			'uses' => 'UserController@c_aprove',
			'as'   => 'user.c_aprove'
	]);	

	Route::get('user/reject/{id}/comment',[
			'uses' => 'UserController@c_reject',
			'as'   => 'user.c_reject'
	]);	

	Route::get('user/aprove/{id}/article',[
			'uses' => 'UserController@a_aprove',
			'as'   => 'user.a_aprove'
	]);	

	Route::get('user/reject/{id}/article',[
			'uses' => 'UserController@a_reject',
			'as'   => 'user.a_reject'
	]);	
	

	// Administration

	Route::get('admin/index',[
			'uses' => 'AdminController@index',
			'as'   => 'admin.index'
	]);	


	Route::get('admin/profiles/index',[
			'uses' => 'AdminController@profiles',
			'as'   => 'admin.profiles'
	]);
	
	// Country
	Route::get('admin/country',[
			'uses' => 'AdminController@country',
			'as'   => 'admin.country'
	]);	

	Route::put('admin/country',[
			'uses' => 'AdminController@country_insert',
			'as'   => 'admin.country.insert'
	]);	
	
	Route::post('admin/country/destroy',[
			'uses' => 'AdminController@country_destroy',
			'as'   => 'admin.country.destroy'
	]);	
	
	// Profesion
	Route::get('admin/profession',[
			'uses' => 'AdminController@profession',
			'as'   => 'admin.profession'
	]);	

	Route::put('admin/profession',[
			'uses' => 'AdminController@profession_insert',
			'as'   => 'admin.profession.insert'
	]);	
	
	Route::post('admin/profession/destroy',[
			'uses' => 'AdminController@profession_destroy',
			'as'   => 'admin.profession.destroy'
	]);	
	
	// City
	Route::get('admin/city',[
			'uses' => 'AdminController@city',
			'as'   => 'admin.city'
	]);	

	Route::put('admin/city',[
			'uses' => 'AdminController@city_insert',
			'as'   => 'admin.city.insert'
	]);	
	
	Route::post('admin/city/destroy',[
			'uses' => 'AdminController@city_destroy',
			'as'   => 'admin.city.destroy'
	]);	
	
	// Perfil
	Route::put('admin/profile/{profile}/update',[
			'uses' => 'AdminController@profile_update',
			'as'   => 'admin.profile.update'
	]);	

	Route::get('admin/profile/{id}/edit',[
			'uses' => 'AdminController@profile_edit',
			'as'   => 'admin.profile.edit'
	]);	
	
	Route::get('admin/config/index',[
			'uses' => 'AdminController@config',
			'as'   => 'admin.config'
	]);	
	
	Route::get('admin/promotion/index',[
			'uses' => 'AdminController@promotion',
			'as'   => 'admin.promotion'
	]);	

	
});

Route::resource('user', 'UserController');	

// Authorization

Route::auth();

Route::get('auth/login', [
		'uses'	=>	'Auth\AuthController@getLogin',
		'as'	=>	'auth.login'
	]);


Route::post('auth/login', [
		'uses'	=>	'Auth\AuthController@postLogin',
		'as'	=>	'auth.login'
	]);


Route::get('auth/logout', [
		'uses'	=>	'Auth\AuthController@getLogout',
		'as'	=>	'auth.logout'
	]);

Route::get('user/activation/{token}', [ 
			'uses'  => 'Auth\AuthController@activateUser',
			'as' 	=> 'user.activate'
	]);



/**
 * used for ajax call 
 *
 */
 
Route::get('ajax-city/{countryid}', function($countryid){
    $city = \App\City::where('countryid', '=', $countryid )->lists('name','cityid');
	
    return Response::json($city);
});

Route::post('ajax-lawyersearch', [
				'uses' => 'LawyerController@search',
				'as' => 'lawyer.search'
]);
	



//Route::get('/home', 'HomeController@index');
