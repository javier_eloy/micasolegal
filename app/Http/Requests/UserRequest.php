<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	
	/**
	* Get message from rules
	*
	*
	*/
	public function messages()
	{
		return [
			'email.required' => 'El correo es requerido',
			'email.unique' => 'El correo está asignado',
			'username.unique' => 'El nombre del usuario está asignado',
			'password.required' => 'La clave de ingreso es requerida',
		];
	}
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$validation = [
            'username' =>  'min:4|max:50|required|unique:t000_login',
			'email' => 'min:4|max:40|required|unique:t000_login',
			'password' => 'min:4|max:20|confirmed|required',
			'image' => 'mimes:jpeg,bmp,png'
        ];

	    if(!is_null(Request::get('is_lawyer')) )
        {
            $validation += [
			   'name' => 'min:4|max:50|required',
			   'lastname' => 'min:4|max:50|required',
			   'address' => 'min:4|max:150|required',
			   'profesionid' => 'required',
			   'resume' => 'max:1000|required'
			];
        }
        return $validation;
    }
}
