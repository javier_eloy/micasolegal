<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
	
	

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$validation = ['image' => 'mimes:jpeg,bmp,png'];
		
		if(Request::get('password') != '')			
      	   $validation += ['password' => 'min:4|max:20|confirmed|required'];

	    if(!is_null(Request::get('is_lawyer')) )
        {
            $validation += [
			   'name' => 'min:4|max:50|required',
			   'lastname' => 'min:4|max:50|required',
			   'address' => 'min:4|max:150|required',
			   'profesionid' => 'required',
			   'resume' => 'max:1000|required'
			];
        }
		return $validation;
    }
}
