<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Laracasts\Flash\Flash;


class ContactFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

	
	/**
	* Get message from rules
	*
	*
	*/
	public function messages()
	{
		return [
			'name.required' => 'El nombre del contacto es requerido',
			'email.required' => 'El correo es requerido',
			'tel.required' => 'El telefono es requerido',			
		];
	}
	
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required',
			'tel' => 'required'
		  ];
    }
}
