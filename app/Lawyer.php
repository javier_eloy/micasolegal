<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Lawyer extends Model
{
    //
	protected $table = 't001_lawyer';
    protected $primaryKey = 'lawyerid';
	
	protected $fillable = ['name','lastname','address','phone','postalcode','profesionid', 'resume','loginid',
							'experience', 'partnership','clientid', 'languageid','service', 'offer_service_id',
							'schedule', 'fareid', 'payment_mode_id', 'payment_type_id'];
			
	
	public function login()
	{
		return $this->belongsTo('App\User','loginid');
	}
		
	public function profesion()
	{
	  return $this->belongsTo('App\Profesion','profesionid');	
	}
	
	public function code()
	{
		return $this->belongstoMany('App\Code','t101_code_lawyers','lawyerid','codeid')->withTimestamps();
	}
	
	public function code_where($filter)
	{
		return $this->belongstoMany('App\Code','t101_code_lawyers','lawyerid','codeid')->wherePivot('type',$filter)->withTimestamps();
	}
	
	
	public function hires()
	{
	  return $this->belongsTo('App\Hire','lawyerid','lawyer_id');	
	}		

	
	public function getShortResumeAttribute()
	{
	   $short=substr($this->resume,0,90);
	   if(strlen($this->resume) > 90) $short.="...";
	   
	   return $short;
		
	}
	
	public function getRateLawyerAttribute()
	{
		return (integer) round($this->hires()->avg('rate'));
	}
	
}
