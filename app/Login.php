<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    //
	protected $table = 't000_login';
	
	protected $fillable = ['username','password','email','roleid','loginid'];

	protected $hidden = ['password'];
	
	public function Lawyer()
	{
	  return $this->hasOne('App\Lawyer');	
	}
	
}
