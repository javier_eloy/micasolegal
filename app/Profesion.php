<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    //
	protected $table = 't008_profesion';
	protected $primaryKey = 'profesionid';
	
	protected $fillable = ['name'];
	
	/*public function Lawyer()
	{
		 return $this->belongsToMany('App\Lawyer','profesionid')->withTimestamps();
	}*/
	
	public function Lawyer()
	{
		 return $this->hasMany('App\Lawyer','profesionid');
	}
	
}
