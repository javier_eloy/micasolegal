<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promotion extends Model
{
    //
	protected $table = 't012_promotion';
	protected $primaryKey = 'promotionid';
	
	protected $fillable = ['name', 'start_date', 'end_date', 'content','loginid','active','discount'];
	
	
	function getStartDateAttribute($value)
	{
		 return  Carbon::parse($value)->format('d/m/Y');
	}
	
	function getEndDateAttribute($value)
	{
		 return  Carbon::parse($value)->format('d/m/Y');
	}

	function getShortContentAttribute()
	{
	  return str_limit($this->content, 100);
	}
	
	function getShortNameAttribute(){
	  return str_limit($this->name, 60);
	}
	
	function user()
	{
		return $this->hasOne('App\User','loginid','loginid');
	}
	
	
	/*public function article()	
	{
	   return $this->hasMany('App\Article','sectionid');
	}*/
	
}
