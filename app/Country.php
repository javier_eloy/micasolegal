<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
	protected $table = 't003_country';
	protected $primaryKey = 'countryid';
	 
	protected $fillable = ['name','initial'];
	
	public function City()
	{
	  return $this->belongsTo('App\City');
	}
	
	public function User()
	{
	 return $this->hasMany('App\User','countryid');	
	}
}
