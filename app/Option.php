<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
	protected $table = 't010_option';
	protected $primaryKey = 'optionid';
	
	protected $fillable = ['type','type_cmod','type_mod','min_levelcanswer', 'min_level_create',
						   'has_section','has_category','has_tags','has_cbestanswer','has_clike' ];
}
