<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
	protected $table = 't005_tag';
	protected $primaryKey = 'tagid';
	
	protected $fillable = ['name'];
	
	public function Article()
	{
		 return $this->belongsToMany('App\Article','t007_article_tag','tagid','articleid')->withTimestamps();
	}
	
}
