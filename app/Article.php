<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
	protected $table = 't006_article';
	protected $primaryKey = 'articleid';
	
	protected $fillable = ['title','content','categoryid','sectionid','type','loginid','image_path'];
	
	public function getShortContent150Attribute()
	{
	  	
      return str_limit(strip_tags($this->content), 150); 	
	}
	public function getShortContent700Attribute()
	{
      return str_limit(strip_tags($this->content), 700); 	
	}	
	
	public function getShortContent500Attribute()
	{
      return str_limit(strip_tags($this->content), 500); 	
	}	

	public function getShortContent300Attribute()
	{
      return str_limit(strip_tags($this->content), 300); 	
	}	
    
	public function getShortTitleAttribute()
	{
	  return str_limit($this->title, 50);
	}
	
	public function getTranslateTypeAttribute()
	{
		 switch($this->type)
		 {
			case ARTICLE_BLOG_MODERATE:
			case ARTICLE_BLOG: 
						$translate = "Articulo";
						break;
			case ARTICLE_QUESTION_MODERATE:
		    case ARTICLE_QUESTION: 
						$translate = "Pregunta";
						break;
			case ARTICLE_FORUM_MODERATE:
			case ARTICLE_FORUM: 
						$translate = "Foro";
						break;
			default: $translate = "(desconocido)";
		 }
		 
		return $translate;
		 
	}
	
	public function category()
	{
		return $this->belongsTo('App\Category','categoryid');
	}
	
	public function section()
	{
		return $this->belongsTo('App\Section','sectionid');
	}
	
	
	public function tags()
	{
		 return $this->belongsToMany('App\Tag','t007_article_tag','articleid','tagid')->withTimestamps();
	}
	
	public function login()
	{
		return $this->belongsTo('App\User','loginid');
	}
	public function comment()
	{
		return $this->hasMany('App\Comment','articleid');
	}
	
	public function scopeTitle($query,$title)
	{
		return $query->where('title', 'LIKE', "%$title%")
					 ->orWhere('content', 'LIKE', "%$title%");
	}
	
	public function scopeCategory($query,$categoryid)
	{

	   return $query->where('categoryid','=',$categoryid);	
	}
	
	public function scopeBlog($query,$hide_queue = false)
	{
	  $fieldset=[ARTICLE_BLOG];
	  if(!$hide_queue) array_push($fieldset,ARTICLE_BLOG_MODERATE);

	  return $query->wherein('type', $fieldset);
	}
	
	public function scopeQuestion($query,$hide_queue = false)
	{
	  $fieldset=[ARTICLE_QUESTION];
	  if(!$hide_queue) array_push($fieldset,ARTICLE_QUESTION_MODERATE);

	  return $query->wherein('type', $fieldset);
	}
	
	
	public function scopeForum($query,$hide_queue = false)
	{
	  $fieldset=[ARTICLE_FORUM];
	  if(!$hide_queue) array_push($fieldset,ARTICLE_FORUM_MODERATE);

	  return $query->wherein('type', $fieldset);
	}
	
	
}
