<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeLawyer extends Model
{
    //
	   //
	protected $table = 't101_code_lawyers';
	protected $primaryKey = 'id';
	
	protected $fillable = ['type','codeid','lawyerid'];
	
	public function scopeClient($query)
	{
		 return $query->where('type','client');
	}
	
	public function scopeLanguage($query)
	{
		 return $query->where('type','language');
	}
	
	public function scopeOfferService($query)
	{
		 return $query->where('type','offer_service');
	}

	public function scopeFare($query)
	{
		 return $query->where('type','fare');
	}

	public function scopePaymentMode($query)
	{
		 return $query->where('type','payment_mode');
	}

	public function scopePaymentType($query)
	{
		 return $query->where('type','payment_type');
	}
	
	public function scopeSpecialities($query)
	{
		 return $query->where('type','special');
	}

}
