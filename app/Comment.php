<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
	protected $table = 't009_comment';
	protected $primaryKey = 'commentid';
	
	protected $fillable = ['text','articleid','loginid','type','verified'];
	
	public function article()
	{
	   return $this->belongsTo('App\Article','articleid');
	}
	
	public function login()
	{
	   return $this->belongsTo('App\User','loginid');
	}
	
	public function getShortTextAttribute()
	{
	  return str_limit($this->text, 50);
	}
	
	

}
