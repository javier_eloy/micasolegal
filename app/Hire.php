<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hire extends Model
{
      //
	protected $table = 't013_hire_lawyer';
    protected $primaryKey = 'user_id';
	
	protected $fillable = ['lawyer_id', 'message','rate'];
	
	
	public function lawyers()	
	{
		return $this->hasOne('App\Lawyer','lawyerid','lawyer_id');	
	}
	public function user()	
	{
		return $this->hasOne('App\User','loginid','user_id');	
	}

}
