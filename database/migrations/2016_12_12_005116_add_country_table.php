<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t003_country', function (Blueprint $table) {
			$table->engine='Innodb'; 
			
		    $table->increments('countryid');
			$table->string('name',50);
			$table->string('initial',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t003_country');
    }
}
