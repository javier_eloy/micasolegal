<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t010_option', function (Blueprint $table) {
            $table->increments('optionid');
			$table->enum('type',array('C','F','A'));
			$table->enum('type_cmod',array('C_MOD_NONE','C_MOD_ADMIN','C_MOD_ADMINOWNER')); // Nivel de moderacion para usar los campos siguientes
			$table->enum('type_mod',array('MOD_NONE','MOD_ADMIN')); // Nivel de moderacion para usar los campos siguientes
			$table->unsignedTinyInteger('min_level_canswer')->default(0); // 0 = todos, 1 = usurios registrados, 2 = abogados
			$table->unsignedTinyInteger('min_level_create')->default(0); // 0 = todos, 1 = usurios registrados, 2 = abogados
			$table->unsignedTinyInteger('has_section')->default(0);
			$table->unsignedTinyInteger('has_category')->default(0);
			$table->unsignedTinyInteger('has_tags')->default(0);
			$table->unsignedTinyInteger('has_cbestanswer')->default(0);
			$table->unsignedTinyInteger('has_clike')->default(0);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t010_option');
    }
}
