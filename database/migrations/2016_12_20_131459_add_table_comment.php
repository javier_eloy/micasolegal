<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t009_comment', function (Blueprint $table) {
            //
			$table->increments('commentid');
			$table->string('text',15000);
			$table->unsignedInteger('articleid');
			$table->unsignedInteger('loginid');			
			$table->enum('type',array('C','C_QUEUED','C_QUEUED_USER')); 			
			$table->unsignedInteger('verified')->default(1);			
			
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t009_comment', function (Blueprint $table) {
            //
        });
    }
}
