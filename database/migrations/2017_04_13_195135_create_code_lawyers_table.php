<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeLawyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t101_code_lawyers', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('codeid');
			$table->unsignedInteger('lawyerid');
			$table->string('type',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t101_code_lawyers');
    }
}
