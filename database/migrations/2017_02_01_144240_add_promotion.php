<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t012_promotion', function (Blueprint $table) {
            //
			$table->increments('promotionid');			
			$table->unsignedInteger('loginid');
			$table->string('name',100);
			$table->string('content',5000);
			$table->smallInteger('discount')->default(0);
			$table->date('start_date');
			$table->date('end_date');
			$table->unsignedTinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t012_promotion', function (Blueprint $table) {
            //
        });
    }
}
