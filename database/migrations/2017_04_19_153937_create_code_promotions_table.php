<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t102_code_promotions', function (Blueprint $table) {
            $table->increments('cpromotionid');
			$table->string('name',50);
			$table->double('cost',5,2);
			$table->boolean('allow_article');
			$table->boolean('default');
			$table->unsignedInteger('n_promo');			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t102_code_promotions');
    }
}
