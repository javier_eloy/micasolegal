<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t006_article', function (Blueprint $table) {
			$table->engine='Innodb';
			
            $table->increments('articleid');
			$table->string('title',255);
			// Q = Consulta, F = Foro, A = Blog o Articulo
			$table->enum('type',array('Q','Q_QUEUED','F','F_QUEUED','A','A_QUEUED')); 
			$table->string('content',15000);
			$table->unsignedInteger('categoryid')->nullable();
			$table->unsignedInteger('sectionid')->nullable();
			$table->integer('loginid');
			$table->string('image_path',200);
            $table->timestamps();
        });
		
	    Schema::create('t007_article_tag', function (Blueprint $table) {
			$table->engine='Innodb';
			
            $table->increments('id');
			$table->unsignedInteger('articleid');
			$table->unsignedInteger('tagid');
			
			$table->foreign('articleid')->references('articleid')->on('t006_article')->onDelete('cascade');
			$table->foreign('tagid')->references('tagid')->on('t005_tag')->onDelete('cascade');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('t007_article_tag');
        Schema::drop('t006_article');
    }
}
