<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLawyerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t001_lawyer', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			
            $table->increments('lawyerid');
			$table->string('name',150);
			$table->string('lastname',150);
			$table->string('address',300)->nullable();
			$table->string('phone',20)->nullable();
			$table->string('postalcode',5)->nullable();
			$table->string('resume',1000)->nullable();
			$table->integer('profesionid');
			$table->unsignedInteger('loginid');
			
			$table->string('experience',50)->nullable(); // Experiencia
			$table->string('partnership',500)->nullable(); // Asociaciones
			$table->string('service',500)->nullable();
			$table->string('schedule',50)->nullable();
			$table->unsignedInteger('rate')->default(0);
			
			//$table->unsignedInteger('clientid')->nullable();
			//$table->unsignedInteger('languageid')->nullable();			
			//$table->unsignedInteger('offer_service_id')->nullable();			
			//$table->unsignedInteger('fareid')->nullable(); // Tarifa
			//$table->unsignedInteger('payment_mode_id')->nullable(); // Facilidades de pagos
			//$table->unsignedInteger('payment_type_id')->nullable(); // Modalidad de pagos
			
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t001_lawyer');
    }
}
