<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t000_login', function (Blueprint $table) {
			$table->engine = 'InnoDB';

            $table->increments('loginid');
			$table->string('username',50)->unique();
			$table->string('password',255);
			$table->string('email',60)->unique();
			$table->string('image_path')->nullable();
			$table->tinyInteger('is_lawyer');
			$table->string('remember_token',100);
			$table->smallInteger('level');
			$table->boolean('activated')->default(false);
			$table->unsignedInteger('countryid');
			$table->unsignedInteger('cityid');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t000_login');
    }
}
