<?php 

if(!defined('ARTICLE_QUESTION')) {
	define('ARTICLE_QUESTION','Q');
}

if(!defined('ARTICLE_QUESTION_MODERATE')) {
	define('ARTICLE_QUESTION_MODERATE','Q_QUEUED');
}

if(!defined('ARTICLE_BLOG')) {
	define('ARTICLE_BLOG','A');
}

if(!defined('ARTICLE_BLOG_MODERATE')) {
	define('ARTICLE_BLOG_MODERATE','A_QUEUED');
}

if(!defined('ARTICLE_FORUM')) {
	define('ARTICLE_FORUM','F');
}

if(!defined('ARTICLE_FORUM_MODERATE')) {
	define('ARTICLE_FORUM_MODERATE','F_QUEUED');
}

if(!defined('COMMENT')) {
	define('COMMENT','C');
}
if(!defined('COMMENT_MODERATE')) {
	define('COMMENT_MODERATE','C_QUEUED');
}

if(!defined('COMMENT_MODERATE_USER')) {
	define('COMMENT_MODERATE_USER','C_QUEUED_USER');
}



// Niveles de usuario para logins

if(!defined('LEVEL_USER')) {
	define('LEVEL_USER','0');
}

if(!defined('LEVEL_LAWYER')) {
	define('LEVEL_LAWYER','100');
}

if(!defined('LEVEL_EDITOR')) {
	define('LEVEL_EDITOR','200');
}

if(!defined('LEVEL_ADMIN')) {
	define('LEVEL_ADMIN','300');
}


// Moderacion


if(!defined('OPT_MOD_NONE')) {
	define('OPT_MOD_NONE','MOD_NONE');
}

if(!defined('OPT_MOD_ADMIN')) {
	define('OPT_MOD_ADMIN','MOD_ADMIN');
}

// Moderacion Respuesta

if(!defined('OPT_CMOD_NONE')) {
	define('OPT_CMOD_NONE','C_MOD_NONE');
}

if(!defined('OPT_CMOD_ADMIN')) {
	define('OPT_CMOD_ADMIN','C_MOD_ADMIN');
}
if(!defined('OPT_CMOD_ADMIN_OWNER')) {
	define('OPT_CMOD_ADMIN_OWNER','C_MOD_ADMINOWNER');
}


return [
  'levels' => [0 => 'Usuario', 200 => 'Editor', 300 => 'Administrador']
];


